import org.jastadd.tree.edit.ASTNodeInterface;
import org.jastadd.tree.edit.JatteCallback;
import org.jastadd.tree.edit.ASTNodeOptInterface;
import org.jastadd.tree.edit.ASTNodeListInterface;
import org.jastadd.tree.edit.Editors;
import org.jastadd.tree.edit.Extractor;
import org.jastadd.tree.edit.CompilerContainer;
import org.jastadd.tree.edit.jtree.ASTNodeInterfaceEditMenuXML;
import org.jastadd.tree.edit.jtree.ASTNodeShowMenu;
import org.jastadd.tree.edit.jtree.ASTOptEditMenuXML;
import org.jastadd.tree.edit.jtree.ASTListEditMenuXML;
import org.jastadd.tree.edit.Visibility;
import java.util.Set;
import java.util.TreeSet;
import java.util.ArrayList;

aspect interfaces {

	ASTNode implements ASTNodeInterface;
	
	List implements ASTNodeListInterface;
	
	Opt implements ASTNodeOptInterface;

	public void ASTNode.setChild(ASTNodeInterface node, int index) {
		setChild((ASTNode) node, index);
	}
	public void ASTNode.insertChild(ASTNodeInterface node, int index) {
		insertChild((ASTNode) node, index);
	}
	

}

aspect defaults {

  syn String ASTNode.ed_default_token(String token) = "FillIn";

}

aspect jtree {
	
 @Override
  public String ASTNode.toString(){
		return ed_label();
  }	
  
  syn String ASTNode.ed_label() {
		String res = "";
		if (this.getParent() == null) {
			res += getClass().getSimpleName();
		} else if (this.getParent() instanceof List || this.getParent() instanceof Opt) {
			res += this.getClass().getSimpleName();
		} else {
			res += Editors.getShowNameFor(this);
		}
		
		for(String token : Extractor.getTokens(this.getClass())){
			String tokenValue = Editors.getValue(this, token);
			res += " <" + token + "=" + tokenValue + ">"; 
		}
		return res;
  } 

}

aspect DnD {
	
	public void ASTNode.ed_accept_resource(Object resource,CompilerContainer cc){
	
	}

	public void ASTNode.ed_upper_accept_resource(Object resource,CompilerContainer cc){
	
	}

	public void ASTNode.ed_lower_accept_resource(Object resource,CompilerContainer cc){
	
	}
	
	syn boolean ASTNode.ed_can_drop(Object resource) = false;
	syn boolean ASTNode.ed_upper_can_drop(Object resource) = false;
	syn boolean ASTNode.ed_lower_can_drop(Object resource) = false;
	
	syn boolean ASTNode.ed_can_drag() = false;

}

aspect ShowNodes {
	syn boolean ASTNode.ed_show() = true;
	 
	syn ArrayList<ASTNode> ASTNode.ed_children_to_show() {
		ArrayList<ASTNode> res = new ArrayList<ASTNode>();
		for(ASTNode n : astChildren()){
			res.addAll(n.ed_to_show());
		}
		return res;
	}
	
	syn ArrayList<ASTNode> ASTNode.ed_to_show() {
		if(ed_show()){
			ArrayList<ASTNode> res = new ArrayList<ASTNode>();
			res.add(this);
			return res;
		}else{
			return ed_children_to_show();
		}
	}
	
	syn ASTNode ASTNode.ed_show_node() = ed_show() ? this : ed_show_parent();
	
	syn ASTNode ASTNode.ed_show_parent() = getParent() != null ? ed_show_parent_inh() : this;
	
	inh ASTNode ASTNode.ed_show_parent_inh();
	
	eq ASTNode.getChild().ed_show_parent_inh() {
		if(ed_show()){
			return this;
		}else{
			return ed_show_parent();
		}
	}
	
	uncache ASTNode.ed_to_show();
	uncache ASTNode.ed_children_to_show();

}

aspect interfaceAs {
	interface Root{ }
	
	eq List.ed_show() = false;
	eq Opt.ed_show() = false;
	eq Root.ed_show() = true;
}

aspect rank {
	syn int ASTNode.ed_rank() = 3;

}
aspect index {

	inh int ASTNode.ed_index(); 
	eq ASTNode.getChild(int i).ed_index() = i;

}
aspect indent {

	inh int ASTNode.ed_indent();
	
	syn int Root.ed_indent() = 0;
	
	eq ASTNode.getChild(int i).ed_indent() {
		if(ed_show()){
			return ed_indent() + 1;
		} else {
			return ed_indent();
		}
	}
	syn int ASTNode.ed_lower_indent() = ed_indent();
	syn int ASTNode.ed_upper_indent() = ed_indent();
}

aspect listVisible {

	syn ArrayList<ASTNode> ASTNode.ed_visible_rows() {
		ArrayList<ASTNode> res = new ArrayList<ASTNode>();
		if(ed_show()){
			res.add(this);
		}
		for(ASTNode n : ed_children_to_show()){
			res.addAll(n.ed_visible_rows());
		}
		return res;
	}
}

aspect lineNumber {
	syn int ASTNode.ed_line_number() {
		if(ed_show()){
			return ed_show_parent().ed_line_number(this) + 1;
		} else {
			return ed_show_parent().ed_line_number();
		}
	}
	
	eq Root.ed_line_number() = 0;
	
	syn int ASTNode.ed_line_number(ASTNode obj) {
		int showIndex = ed_children_to_show().indexOf(obj);
		if(showIndex == 0){
			return ed_line_number();
		} else {
			return ed_children_to_show().get(showIndex - 1).ed_last_line_number();
		}
	}

	syn int ASTNode.ed_last_line_number(){
		int size = ed_children_to_show().size();
		if(size == 0){
			return ed_line_number();
		}else {
			return ed_children_to_show().get(size - 1).ed_last_line_number();
		}
	}
}

aspect errors {
	coll Set<String> ASTNode.ed_line_errors() [new TreeSet<String>()] with add;
	// Detta funkar inte för att det finns flera rötter. // Löst i en nyare version av JastAdd
}

aspect printTree {

//	public void ASTNode.printTree(String indent) {
//		System.out.println(indent + this);
//		for( ASTNode node : astChildren()){
//			node.printTree(indent + "    ");
//		}
//	}

}