package org.jastadd.tree.edit;

import java.awt.Component;

public interface MenuInterface extends ASTNodeInterface{
	
	Component ed_generate_menu(CompilerContainer cc);

}
