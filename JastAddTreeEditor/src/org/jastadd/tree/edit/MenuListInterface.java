package org.jastadd.tree.edit;

public interface MenuListInterface extends MenuInterface {
	
	Iterable<? extends MenuInterface> getMenus();
	

}
