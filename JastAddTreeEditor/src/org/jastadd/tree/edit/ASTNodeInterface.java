package org.jastadd.tree.edit;

import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Set;

public interface ASTNodeInterface {
	
	ASTNodeInterface getParent();

	int getNumChild();

	ASTNodeInterface getChild(int i);

	void setChild(ASTNodeInterface node, int index);

	String ed_default_token(String token);

	String ed_label();
	
	void removeChild(int res);
	
	void ed_right_click(CompilerContainer cc, MouseEvent me);

	void ed_accept_resource(Object resource, CompilerContainer cc);
	
	boolean ed_can_drag();
	
	boolean ed_can_drop(Object resource);
	
	void flushTreeCache();
	
	ArrayList<? extends ASTNodeInterface> ed_to_show();
	
	ArrayList<? extends ASTNodeInterface> ed_children_to_show();
	
	ArrayList<? extends ASTNodeInterface> ed_visible_rows();
	
	ASTNodeInterface ed_show_parent();

	boolean ed_show();
	
	MenuContainerInterface ed_menu(CompilerContainer cc);
	
	Iterable<? extends ASTNodeInterface> astChildren();

	int ed_rank();

	int ed_indent();
	
	public void insertChild(ASTNodeInterface node, int i);
	
	public ASTNodeInterface treeCopy();
	
	LabelFormatLineInterface ed_label_format();

	boolean ed_upper_can_drop(Object o);

	void ed_upper_accept_resource(Object data, CompilerContainer cc);

	boolean ed_lower_can_drop(Object o);

	void ed_lower_accept_resource(Object data, CompilerContainer cc);

	int ed_upper_indent();

	int ed_lower_indent();
	
	int ed_line_number();

	int ed_last_line_number();
	
	Set<String> ed_line_errors();
	
}
