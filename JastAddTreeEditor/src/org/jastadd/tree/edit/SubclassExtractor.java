package org.jastadd.tree.edit;

import static org.jastadd.tree.edit.Extractor.*;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Comparator;

import org.jastadd.tree.edit.exception.JastAddTreeEditException;


public class SubclassExtractor {
	private int count = 0;
	private String path;
	
	public SubclassExtractor(String path){
		this.path = path;
	}
	

	public Class<?>[] getSubClasses(Class<?> o) {
		InputStream content = getClass().getResourceAsStream("/dir.txt");
		BufferedReader is;
		if (content != null) {
			is = new BufferedReader(new InputStreamReader(content));
		} else {
			try {
				is = new BufferedReader(new FileReader(path));
			} catch (FileNotFoundException e) {
				throw new JastAddTreeEditException(e);
			}
		}
		ArrayList<Class<?>> res = new ArrayList<Class<?>>();
		String pack = o.getPackage().getName();
		try {
			while (is.ready()) {
				String fileName = is.readLine();
					Object tmp;
					try {
						tmp = Class.forName(pack + "." + fileName.substring(0, fileName.length() - 5))
								.newInstance();
						if (o.isInstance(tmp)) {
							res.add(tmp.getClass());
						}
					} catch (InstantiationException e) {
					} catch (IllegalAccessException e) {
					} catch (ClassNotFoundException e) {
					}
					
			}
		} catch (IOException e1) {
				throw new JastAddTreeEditException(e1);
		}
		return res.toArray(new Class<?>[0]);
	}

	public void inferAll(ASTNodeInterface node) {
		for (int i = 1; i < 10; i++) {
			if (inferAll(node, i)) {
				return;
			}

		}
		throw new JastAddTreeEditException("To complex abstract grammar can not infer all members");

	}

	private boolean inferAll(ASTNodeInterface node, int level) {
		if (level == 0) {
			return false;
		}

		for (String childName : getChild(node.getClass())) {
			if (!canCreateChild(childName, node, level)) {
				return false;
			}
		}
		fillTokens(node);

		return true;

	}

	private boolean canCreateChild(String childName, ASTNodeInterface node, int level) {
		Class<?>[] typesOfChildren = getTypesOfChildren(childName, node);

		ArrayList<ASTNodeInterface> childCandidates = getChildCandidates(typesOfChildren);

		for (ASTNodeInterface child : childCandidates) {
			if (getChild(child.getClass()).length == 0) {
				fillTokens(child);
				setValue(node, childName, child);
				return true;

			} else {
				boolean res = inferAll(child, level - 1);
				if (res) {
					fillTokens(child);
					setValue(node, childName, child);
					return true;
				}

			}

		}
		return false;
	}

	private ArrayList<ASTNodeInterface> getChildCandidates(Class<?>[] typesOfChildren) {
		ArrayList<ASTNodeInterface> childCandidates = new ArrayList<ASTNodeInterface>();
		for (Class<?> c : typesOfChildren) {
			try {
				childCandidates.add((ASTNodeInterface) c.newInstance());
			} catch (InstantiationException e) {
				throw new JastAddTreeEditException("Can not create instance of: " + c.getName(), e);
			} catch (IllegalAccessException e) {
				throw new JastAddTreeEditException("Can not create instance of: " + c.getName(), e);
			}
		}
		childCandidates.sort(new Comparator<ASTNodeInterface>() {
			@Override
			public int compare(ASTNodeInterface o1, ASTNodeInterface o2) {
				
				int rank = Integer.compare(o1.ed_rank(), o2.ed_rank());
				if (rank != 0) {
					return rank;
				}
				
				int child = Integer.compare(getChild(o1.getClass()).length, getChild(o2.getClass()).length);
				if (child != 0) {
					return child;
				}
				int token = Integer.compare(getTokens(o1.getClass()).length, getTokens(o2.getClass()).length);
				if (token != 0) {
					return token;
				}
				int list = Integer.compare(getList(o1.getClass()).length, getList(o2.getClass()).length);
				if (list != 0) {
					return list;
				}
				return Integer.compare(getOpt(o1.getClass()).length, getOpt(o2.getClass()).length);
			}

		});
		return childCandidates;
	}

	private void setValue(ASTNodeInterface node, String childName, ASTNodeInterface child) {
		Method m;
		try {
			m = node.getClass().getMethod("set" + childName, getReturnType(childName, node));
			m.invoke(node, child); // Maybe have a default value
		} catch (NoSuchMethodException e) {
			throw new JastAddTreeEditException("Can not call method: set" + childName, e);
		} catch (SecurityException e) {
			throw new JastAddTreeEditException("Can not call method: set" + childName, e);
		} catch (IllegalAccessException e) {
			throw new JastAddTreeEditException("Can not call method: set" + childName, e);
		} catch (IllegalArgumentException e) {
			throw new JastAddTreeEditException("Can not call method: set" + childName, e);
		} catch (InvocationTargetException e) {
			throw new JastAddTreeEditException("Can not call method: set" + childName, e);
		}
	}

	private void fillTokens(ASTNodeInterface child) {
		for (String token : getTokens(child.getClass())) {
			try {
				String fillIn = child.ed_default_token(token);
				Method m = child.getClass().getMethod("set" + token, String.class);
				m.invoke(child, fillIn);
			} catch (NoSuchMethodException e) {
				throw new JastAddTreeEditException("Can not call method: set" + token, e);
			} catch (SecurityException e) {
				throw new JastAddTreeEditException("Can not call method: set" + token, e);
			} catch (IllegalAccessException e) {
				throw new JastAddTreeEditException("Can not call method: set" + token, e);
			} catch (IllegalArgumentException e) {
				throw new JastAddTreeEditException("Can not call method: set" + token, e);
			} catch (InvocationTargetException e) {
				throw new JastAddTreeEditException("Can not call method: set" + token, e);
			} catch (Exception e) {
				throw new JastAddTreeEditException("Can not call method: set: Other problem" + token, e);
			}

		}

	}

	private Class<?>[] getTypesOfChildren(String childName, ASTNodeInterface node) {
		return getSubClasses(getReturnType(childName, node));
	}

	private Class<?> getReturnType(String childName, ASTNodeInterface node) {
		try {
			Method m = node.getClass().getMethod("get" + childName);
			Class<?> type = m.getReturnType();
			return type;
		} catch (NoSuchMethodException e) {
			throw new JastAddTreeEditException("Can not find mentod get" + childName, e);
		} catch (SecurityException e) {
			throw new JastAddTreeEditException("Can not find mentod get" + childName, e);
		}
	}

}
