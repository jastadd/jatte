package org.jastadd.tree.edit.xml;

import java.io.IOException;

import internal.org.kxml2.io.KXmlSerializer;

public class OptConstructorAnnotation extends ASTConstructorAnnotation {

	public OptConstructorAnnotation(String name, String type) {
		super(name, type);
	}

	@Override
	void emit(Object root, KXmlSerializer serializer) throws IOException {
		Object child = ChildConstructorAnnotation.getChild(root, name() + "Opt" );
		serializer.startTag(null, "Opt");
		serializer.attribute(null, "name", name());
		if (ListConstructorAnnotation.getNumChild(child) > 0) {
			Object obj = ListConstructorAnnotation.getChild(child, 0);
			ChildConstructorAnnotation.emitChild(serializer, obj, name(),type().substring(4, type().length()-1)); 
		}
		serializer.endTag(null, "Opt");
	}

}
