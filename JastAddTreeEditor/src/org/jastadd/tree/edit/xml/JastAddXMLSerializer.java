package org.jastadd.tree.edit.xml;

import java.io.IOException;
import java.io.StringWriter;

import internal.org.kxml2.io.KXmlSerializer;

public class JastAddXMLSerializer {

	public static String serialize(Object root) throws IOException{
		return serializ(root);
	}
	
	public static String serializ(Object root) throws IOException{
		KXmlSerializer serializer = new KXmlSerializer();
		StringWriter sw = new StringWriter();
		serializer.setOutput(sw);
		serializer.startDocument(null, null);
		
		ASTConstructorAnnotation.serialize(root,serializer);

		serializer.endDocument();
		return sw.toString();
	}
}
