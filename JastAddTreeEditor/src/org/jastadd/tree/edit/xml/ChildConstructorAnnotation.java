package org.jastadd.tree.edit.xml;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

import org.jastadd.tree.edit.exception.JastAddTreeEditException;

import internal.org.kxml2.io.KXmlSerializer;

public class ChildConstructorAnnotation extends ASTConstructorAnnotation {

	public ChildConstructorAnnotation(String name, String type) {
		super(name, type);
	}

	@Override
	void emit(Object parent, KXmlSerializer serializer) throws IOException  {
		Object child = getChild(parent,name());
		emitChild(serializer, child,name(),type());
	}

	static void emitChild(KXmlSerializer serializer, Object child, String name,String type) throws IOException {
		Class<? extends Object> classType = child.getClass();
		
		serializer.startTag(null, classType.getSimpleName());
		serializer.attribute(null, "name", name);  
		serializer.attribute(null, "type", type);  
		List<ASTConstructorAnnotation> l = ASTConstructorAnnotation.getConstructorParameters(child.getClass());
		for (ASTConstructorAnnotation astConstructorAnnotation : l) {
			astConstructorAnnotation.emit(child, serializer);
		}
		serializer.endTag(null, classType.getSimpleName());
	}

	static Object getChild(Object root,String name) {
		Object child = null;
		try {
			Method getValue = root.getClass().getMethod("get" + name);
			child = getValue.invoke(root);
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			throw new JastAddTreeEditException("Error in: get" + name + " at " + root , e);
		}
		return child;
	}
	
}
