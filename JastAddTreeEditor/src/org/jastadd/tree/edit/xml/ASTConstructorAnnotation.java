package org.jastadd.tree.edit.xml;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import org.jastadd.tree.edit.exception.JastAddTreeEditException;

import internal.org.kxml2.io.KXmlSerializer;

public abstract class ASTConstructorAnnotation {

	public static void serialize(Object root, KXmlSerializer serializer) {
		try {
			serializer.startTag(null, root.getClass().getSimpleName());
			List<ASTConstructorAnnotation> l = getConstructorParameters(root.getClass());
			for (ASTConstructorAnnotation astConstructorAnnotation : l) {
				astConstructorAnnotation.emit(root, serializer);
			}
			serializer.endTag(null, root.getClass().getSimpleName());
		} catch (IOException e) {
			throw new JastAddTreeEditException("IO error", e);
		}

	}

	abstract void emit(Object root, KXmlSerializer serializer) throws IOException;

	public static List<ASTConstructorAnnotation> getConstructorParameters(Class<?> cls) {
		List<ASTConstructorAnnotation> res = new ArrayList<>();

		Annotation theConstructorAnnotation = getAnnotation(cls);
		if (theConstructorAnnotation == null) {
			return res;
		}

		String[] names = extractAnnotationList(theConstructorAnnotation, "name");
		String[] types = extractAnnotationList(theConstructorAnnotation, "type");
		String[] kinds = extractAnnotationList(theConstructorAnnotation, "kind");

		for (int i = 0; i < kinds.length; i++) {
			switch (kinds[i]) {
			case "Child":
				res.add(new ChildConstructorAnnotation(names[i], types[i]));
				break;
			case "Opt":
				res.add(new OptConstructorAnnotation(names[i], types[i]));
				break;
			case "List":
				res.add(new ListConstructorAnnotation(names[i], types[i]));
				break;
			case "Token":
				res.add(new TokenConstructorAnnotation(names[i], types[i]));
				break;
			}
		}

		return res;
	}

	private static <E extends ASTConstructorAnnotation> List<E> getConstructorOfKind(String kind, Class<?> cls) {
		List<E> res = new ArrayList<>();

		Annotation theConstructorAnnotation = getAnnotation(cls);
		if (theConstructorAnnotation == null) {
			return res;
		}

		String[] names = extractAnnotationList(theConstructorAnnotation, "name");
		String[] types = extractAnnotationList(theConstructorAnnotation, "type");
		String[] kinds = extractAnnotationList(theConstructorAnnotation, "kind");

		for (int i = 0; i < kinds.length; i++) {

			if (kind.equals(kinds[i])) {
				switch (kinds[i]) {
				case "Child":
					ChildConstructorAnnotation tmp = new ChildConstructorAnnotation(names[i], types[i]);
					res.add((E) tmp);
					break;
				case "Opt":
					OptConstructorAnnotation tmp1 = new OptConstructorAnnotation(names[i], types[i]);
					res.add((E) tmp1);
					break;
				case "List":
					ListConstructorAnnotation tmp2 = new ListConstructorAnnotation(names[i], types[i]);
					res.add((E) tmp2);
					break;
				case "Token":
					TokenConstructorAnnotation tmp3 = new TokenConstructorAnnotation(names[i], types[i]);
					res.add((E) tmp3);
					break;
				}
			}
		}

		return res;
	}

	private static String[] extractAnnotationList(Annotation theConstructorAnnotation, String methodName) {
		String[] names = null;
		try {
			Method getNames = theConstructorAnnotation.getClass().getMethod(methodName);
			names = (String[]) getNames.invoke(theConstructorAnnotation);
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			e.printStackTrace();
			throw new JastAddTreeEditException("Can not extract method", e);
		}
		return names;
	}

	private static Annotation getAnnotation(Class<?> cls) {
		Constructor<?>[] constructors = cls.getConstructors();
		for (Constructor<?> constructor : constructors) {
			Annotation[] annotations = constructor.getAnnotations();
			for (Annotation annotation : annotations) {
				String annotationName = annotation.annotationType().getSimpleName();
				if (annotationName.equals("Constructor")) {
					return annotation;
				}
			}
		}
		return null;
	}

	private String name;
	private String type;

	public ASTConstructorAnnotation(String name, String type) {
		this.name = name;
		this.type = type;
	}

	public String name() {
		return name;
	}

	public String type() {
		return type;
	}

	public static List<TokenConstructorAnnotation> getTokenConstructorAnnotation(Class<?> class1) {
		return getConstructorOfKind("Token",class1);
	}

	public static List<ListConstructorAnnotation> getListConstructorAnnotation(Class<?> class1) {
		return getConstructorOfKind("List",class1);
	}

	public static List<ChildConstructorAnnotation> getChildConstructorAnnotation(Class<?> class1) {
		return getConstructorOfKind("Child",class1);
	}

	public static List<OptConstructorAnnotation> getOptConstructorAnnotation(Class<?> class1) {
		return getConstructorOfKind("Opt",class1);
	}

}
