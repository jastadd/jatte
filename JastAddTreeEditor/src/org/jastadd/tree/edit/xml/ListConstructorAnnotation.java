package org.jastadd.tree.edit.xml;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.jastadd.tree.edit.exception.JastAddTreeEditException;

import internal.org.kxml2.io.KXmlSerializer;

public class ListConstructorAnnotation extends ASTConstructorAnnotation {

	public ListConstructorAnnotation(String name, String type) {
		super(name, type);
	}

	@Override
	void emit(Object root, KXmlSerializer serializer) throws IOException {
		Object child = ChildConstructorAnnotation.getChild(root, name() + "List" );
		serializer.startTag(null, "List");
		serializer.attribute(null, "name", name());
		
		for (int i = 0; i <getNumChild(child); i++) {
			Object obj = getChild(child, i);
			ChildConstructorAnnotation.emitChild(serializer, obj, name() + "[" + i + "]",type().substring(5, type().length() - 1));
		}
		serializer.endTag(null, "List");
	}
	
	static int getNumChild(Object obj){
		Class<? extends Object> theClass = obj.getClass();
		int res = 0;
		try {
			Method method = theClass.getMethod("getNumChild");
			Integer tmp = (Integer) method.invoke(obj);
			res = tmp.intValue();
			
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			throw new JastAddTreeEditException("Error in reflexive getNumChild for " + obj, e);
		}
		return res;
		
	}
	
	static Object getChild(Object obj,int index){
		Class<? extends Object> theClass = obj.getClass();
		Object res = null;
		try {
			Method method = theClass.getMethod("getChild",int.class);
			res =  method.invoke(obj,index);
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			System.out.println(obj);
			e.printStackTrace();
			throw new JastAddTreeEditException("Error in reflexive getChild(" + index + ") for " + obj, e);
		}
		return res;
	}
}
