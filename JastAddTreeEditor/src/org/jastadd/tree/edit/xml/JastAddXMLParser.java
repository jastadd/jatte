package org.jastadd.tree.edit.xml;

import java.io.IOException;
import java.io.StringReader;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

import org.jastadd.tree.edit.exception.JastAddTreeEditException;

import internal.org.kxml2.io.KXmlParser;
import internal.org.xmlpull.v1.XmlPullParserException;

public class JastAddXMLParser {

	private KXmlParser parser;
	private String nameSpace;

	public JastAddXMLParser(String xmlData, String nameSpace) throws XmlPullParserException {
		parser = new KXmlParser();
		parser.setInput(new StringReader(xmlData));
		this.nameSpace = nameSpace;
	}

	public Object parse() throws XmlPullParserException, IOException {
		parser.nextTag();
		return parseChild();
	}

	private Object parseChild() throws XmlPullParserException, IOException {
		String type = parser.getName();
		Object res = createObject(type, nameSpace);
		while (parser.nextTag() != KXmlParser.END_TAG) {
			parseSomething(res);
		}
		return res;
	}

	private Object parseChild(Object parent) throws XmlPullParserException, IOException {
		String type = parser.getName();
		String name = parser.getAttributeValue(null, "name");
		String parentType = parser.getAttributeValue(null, "type");
		Object res = createObject(type, nameSpace);
		set(parent, name, res, parentType);
		while (parser.nextTag() != KXmlParser.END_TAG) {
			parseSomething(res);
		}
		return res;
	}

	private void set(Object node, String fiels, Object arg, String parentType) {
		Class<? extends Object> theClass = node.getClass();
		try {
			Method method = theClass.getMethod("set" + fiels, Class.forName(nameSpace + "." + parentType));
			method.invoke(node, arg);
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException | ClassNotFoundException e) {
			//e.printStackTrace();
			//throw new JastAddTreeEditException("Error in reflexive set" + fiels + " with parent: " + parentType, e);
		}

	}

	private void setToken(Object node, String fiels, Object arg) {
		Class<? extends Object> theClass = node.getClass();
		try {
			Method method = theClass.getMethod("set" + fiels, String.class);
			method.invoke(node, arg);
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			e.printStackTrace();
			throw new JastAddTreeEditException("Error in reflexive set" + fiels + " with parent: String", e);
		}

	}

	private Object parseSomething(Object parent) throws XmlPullParserException, IOException {
		String name = parser.getName();
		try {
			if (name.equals("String")) {
				return parseString(parent);
			} else if (name.equals("Opt")) {
				return parseOpt(parent);
			} else if (name.equals("List")) {
				return parseList(parent);
			} else {
				return parseChild(parent);
			}
		} catch (JastAddTreeEditException e) {
			throw new JastAddTreeEditException( "Error with name " + name + " whith parent: " + parent ,e);
		}
	}

	private Object parseList(Object parent) throws XmlPullParserException, IOException {
		String type = parser.getName();
		String name = parser.getAttributeValue(null, "name");
		Object res = createObject(type, nameSpace);
		set(parent, name + "List", res, "List");
		ArrayList<Object> args = new ArrayList<Object>();
		while (parser.nextTag() != KXmlParser.END_TAG) {
			args.add(parseChild());
		}
		for (int i = 0; i < args.size(); i++) {
			insertChild(res, args.get(i), i, nameSpace);
		}
		return res;
	}

	private Object parseOpt(Object parent) throws XmlPullParserException, IOException {
		String type = parser.getName();
		String name = parser.getAttributeValue(null, "name");
		Object res = createObject(type, nameSpace);
		set(parent, name + "Opt", res, "Opt");
		ArrayList<Object> args = new ArrayList<Object>();
		while (parser.nextTag() != KXmlParser.END_TAG) {
			args.add(parseChild());
		}
		for (int i = 0; i < args.size(); i++) {
			insertChild(res, args.get(i), i, nameSpace);
		}
		return res;
	}

	private static void insertChild(Object node, Object arg, int i, String nameSpace) {
		Class<? extends Object> theClass = node.getClass();
		try {
			Class<?> cls = Class.forName(nameSpace + "." + "ASTNode");
			Method method = theClass.getMethod("insertChild", cls, int.class);
			method.invoke(node, arg, i);
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException | ClassNotFoundException e) {
			e.printStackTrace();
			throw new JastAddTreeEditException("Error in reflexive getChild", e);
		}
	}

	private Object parseString(Object parent) throws XmlPullParserException, IOException {
		String res = parser.getAttributeValue(null, "value");
		String name = parser.getAttributeValue(null, "name");
		setToken(parent, name, res);
		parser.nextTag();
		return res;
	}

	public static Object createObject(String name, String nameSpace) {
		try {
			return Class.forName(nameSpace + "." + name).newInstance();
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			throw new JastAddTreeEditException("Error in create object " + name, e);

		}
	}

	public static Object parse(String serializ, String string) throws XmlPullParserException, IOException {
		JastAddXMLParser tmp = new JastAddXMLParser(serializ, string);
		return tmp.parse();
	}

}
