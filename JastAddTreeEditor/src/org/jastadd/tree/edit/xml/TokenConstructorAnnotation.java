package org.jastadd.tree.edit.xml;

import java.io.IOException;

import internal.org.kxml2.io.KXmlSerializer;

public class TokenConstructorAnnotation extends ASTConstructorAnnotation {

	public TokenConstructorAnnotation(String name, String type) {
		super(name, type);
	}

	@Override
	void emit(Object root, KXmlSerializer serializer) throws IOException {
		 String value = (String) ChildConstructorAnnotation.getChild(root, name());
		 serializer.startTag(null, "String");
		 serializer.attribute(null, "value", value == null ? "" : value);
		 serializer.attribute(null, "name", name());
		 serializer.endTag(null, "String");
	}

}
