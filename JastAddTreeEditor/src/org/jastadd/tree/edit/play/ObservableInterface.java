package org.jastadd.tree.edit.play;

import java.util.ArrayList;

public interface ObservableInterface {
	
	public ArrayList<ObserverInterface> listeners = new ArrayList<>();
	
	default public void addListener(ObserverInterface obj) {
		listeners.add(obj);
	}
	
	default public void removeListner(ObserverInterface obj){
		while(listeners.remove(obj))
			;
	}
	
	default public void notifyAllListners(){
		for(ObserverInterface listner : listeners){
			listner.notifyListner(this);
		}
	}

}
