package org.jastadd.tree.edit;

import java.util.ArrayList;
import java.util.Observable;

import org.jastadd.tree.edit.play.ObservableInterface;


public class CompilerContainer extends Observable {
	
	
	private SubclassExtractor se;
	private ArrayList<ASTNodeInterface> changedNodes;
	private boolean dragging = false;
	private Object currentDrag = null;
	
	private ArrayList<DragListner> dragListners = new ArrayList<>();
	
	public void addDragListner(DragListner dragListner) {
		dragListners.add(dragListner);
	}
	
	public void removeDragListner(DragListner dragListner) {
		while(dragListners.remove(dragListner))
			;
	}
	
	public void clearDragListners() {
		dragListners.clear();
	}
	
	public CompilerContainer(SubclassExtractor se) {
		this.se = se;
		 changedNodes = new ArrayList<>();
	}


	public void hasChangeTree() {
		setChanged();
		notifyObservers();
		changedNodes.clear();
	}

	public SubclassExtractor getSubclassExtractor() {
		return se;
	}
	
	
	public void addChangedNode(ASTNodeInterface node){
		changedNodes.add(node);
	}
	
	public ArrayList<ASTNodeInterface> getChangesNodes(){
		return changedNodes;
	}
	
	public void startedDrag(Object node){
		currentDrag = node;
		dragging = true;
		for(DragListner dl : dragListners){
			dl.begunDrag(node);
		}
	}
	
	public void stopedDrag(){
		currentDrag = null;
		dragging = false;
		for(DragListner dl : dragListners){
			dl.endedDrag();
		}
	}
	
	public boolean dragging() {
		return dragging;
	}
	
	public Object currentDragging() {
		return currentDrag;
	}

	public void preRenderAction(ASTNodeInterface astNode) {
		
	}
	

}
