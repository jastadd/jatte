package org.jastadd.tree.edit;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.jastadd.tree.edit.exception.JastAddTreeEditException;

public class Editors {
	public static ASTNodeInterface addToList(final ASTNodeListInterface list, ASTNodeInterface node) {
		list.setChild(node, list.getNumChild());
		return node;
	}
	public static ASTNodeInterface addFirstToList(final ASTNodeListInterface list, ASTNodeInterface node) {
//		list.setChild(node, list.getNumChild());
		list.insertChild(node, 0);
		return node;
	}


	public static ASTNodeInterface addToList(final ASTNodeListInterface list, Class<?> toCreate,
			SubclassExtractor se) {
		ASTNodeInterface node;
		try {
			node = (ASTNodeInterface) toCreate.newInstance();
			se.inferAll(node);
			list.setChild(node, list.getNumChild());
		} catch (InstantiationException | IllegalAccessException e) {
			throw new JastAddTreeEditException("Error in add to List:" + list, e);
		}
		return node;
	}
	public static ASTNodeInterface addToList(final ASTNodeListInterface list, Class<?> toCreate,
			SubclassExtractor se,int index) {
		ASTNodeInterface node;
		try {
			node = (ASTNodeInterface) toCreate.newInstance();
			se.inferAll(node);
			list.insertChild(node, index);
			
		} catch (InstantiationException | IllegalAccessException e) {
			throw new JastAddTreeEditException("Error in add to List:" + list, e);
		}
		return node;
	}

	public static void editChild(ASTNodeInterface node, String name, Class<?> itm, SubclassExtractor se) {
		try {

			ASTNodeInterface o = (ASTNodeInterface) itm.newInstance();
			se.inferAll(o);
			Method get = node.getClass().getMethod("get" + name);
			Method set = node.getClass().getMethod("set" + name, get.getReturnType());
			ASTNodeInterface res = (ASTNodeInterface) get.invoke(node);
			set.invoke(node, o);
		} catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException
				| IllegalArgumentException | InvocationTargetException e) {
			throw new JastAddTreeEditException("Error in edit child:" + node + " has name " + name, e);
		}
	}

	public static void setValue(String token, String val, ASTNodeInterface node) {
		try {
			Method mot = node.getClass().getMethod("set" + token, String.class);
			mot.invoke(node, val);
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			e.printStackTrace();
			throw new JastAddTreeEditException("Can not find method: " + "set" + token + " on " + node, e);
		}
	}
	
	public static String getRegex(String token, ASTNodeInterface node) {
		try {
			Method mot = node.getClass().getMethod("regex" + token);
			Object res = mot.invoke(node);
			return (String) res;
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			return null;
		}
	}

	public static void removeOpt(ASTNodeInterface node, String name) {
		try {
			Method get2 = node.getClass().getMethod("get" + name);
			Method get = node.getClass().getMethod("get" + name + "Opt");
			Method set = node.getClass().getMethod("set" + name + "Opt", get.getReturnType());
			Class<?> tmp0 = get.getReturnType();
			ASTNodeInterface n = (ASTNodeInterface) get2.invoke(node);
			set.invoke(node, tmp0.newInstance());
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException | InstantiationException e) {
			throw new JastAddTreeEditException("Error in remove Opt: " + node + " has name " + name, e);
		}
	}

	public static void addOpt(ASTNodeInterface node, String name, Class<?> toCreate, SubclassExtractor se) {
		try {
			Method get = node.getClass().getMethod("get" + name + "Opt");
			Method set = node.getClass().getMethod("set" + name + "Opt", get.getReturnType());
			Class<?> tmp0 = get.getReturnType();
			Constructor<?>[] tmp = tmp0.getConstructors();
			Constructor<?> tmp1 = null;
			for (Constructor<?> t : tmp) {
				if (t.getParameterTypes().length == 1) {
					tmp1 = t;
				}
			}
			ASTNodeInterface arg1 = (ASTNodeInterface) toCreate.newInstance();
			se.inferAll(arg1);

			Object tmp2 = tmp1.newInstance(arg1);
			set.invoke(node, tmp2);
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException | InstantiationException e) {
			throw new JastAddTreeEditException("Error in adding opt on: " + node + " has name " + name, e);
		}
	}


	public static boolean hasOpt(ASTNodeInterface node, String optName) {
		boolean isSet = false;
		try {
			Method m1 = node.getClass().getMethod("has" + optName);
			 isSet = (boolean) m1.invoke(node);
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			throw new JastAddTreeEditException("Error in hasOpt: " + node + " has name: " + optName, e);
		}
		return isSet;
	}
	
	public static Class<?>[] getClasses(ASTNodeInterface node, String name,SubclassExtractor sce) {
		Class<?>[] classes = null;
		try {
			Method m = node.getClass().getMethod("get" + name);
			classes = sce.getSubClasses(m.getReturnType());
			
		} catch (NoSuchMethodException | SecurityException | IllegalArgumentException e) {
			throw new JastAddTreeEditException("Error in getting classes from: " + node + " that has name " + name + "WARNING!!! can be NTA, redfine ed_menu for NTA.", e);
		}
		return classes;
	}
	
	public static ASTNodeListInterface extractItems(ASTNodeInterface node, String listName) {
		final ASTNodeListInterface list;
		try {
			Method m = node.getClass().getMethod("get" + listName + "List");
			list = (ASTNodeListInterface) m.invoke(node);
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			e.printStackTrace();
			throw new JastAddTreeEditException("Error in extracting items from: " + node + " in list named: " + listName, e);
		}
		return list;
	}
	
	public static ASTNodeOptInterface extractItemOpt(ASTNodeInterface node, String optName) {
		final ASTNodeOptInterface opt; //Fixa här
		try {
			Method m = node.getClass().getMethod("get" + optName + "Opt");
			opt = (ASTNodeOptInterface) m.invoke(node);
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			e.printStackTrace();
			throw new JastAddTreeEditException("Error in extracting items from: " + node + " in opt named: " + optName, e);
		}
		return opt;
	}
	
	public static ASTNodeInterface extractItem(ASTNodeInterface node, String child) {
		final ASTNodeInterface res;
		try {
			Method m = node.getClass().getMethod("get" + child);
			res = (ASTNodeInterface) m.invoke(node);
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			throw new JastAddTreeEditException("Error in extract item on: " + node + " whith name: " + child, e);
		}
		return res;
	}

	public static Class<?>[] getClassesList(ASTNodeInterface node, String name, SubclassExtractor sce) {
		Class<?>[] classes = null;
		try {
			Method m = node.getClass().getMethod("get" + name,int.class);
			classes = sce.getSubClasses(m.getReturnType());
			
		} catch (NoSuchMethodException | SecurityException | IllegalArgumentException e) {
			throw new JastAddTreeEditException("Error in getting List: get" + name + " when this is " + node.getClass().getSimpleName(), e);
		}
		return classes;
	}
	public static Class<?> getClassList(ASTNodeInterface node, String name) {
		try {
			Method m = node.getClass().getMethod("get" + name,int.class);
			return m.getReturnType();
			
		} catch (NoSuchMethodException | SecurityException | IllegalArgumentException e) {
			throw new JastAddTreeEditException("Error in getting List: get" + name + " when this is " + node.getClass().getSimpleName(), e);
		}
	}
	
	public static String getValue(ASTNodeInterface node,String token) {
		try {
			Method mot = node.getClass().getMethod("get" + token);
			Object o = mot.invoke(node);
			if ( o instanceof String){
				return (String) o;
			} else {
				return node.getClass().getSimpleName();
				
			}
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			e.printStackTrace();
			throw new JastAddTreeEditException("Can not find method: " + "get" + token, e);
		}
	}
	
	public static Class<?> getCurrentClass(ASTNodeInterface node,String name){
		try {
			Method m = node.getClass().getMethod("get" + name);
			Object o = m.invoke(node);
			return o.getClass();
		} catch (NoSuchMethodException | SecurityException | IllegalArgumentException | IllegalAccessException | InvocationTargetException e) {
			throw new JastAddTreeEditException("Error in getting current class of: " + name + " in " + node, e);
		}	
	}

	public static String getListNameFor(ASTNodeInterface node) {
		if(!(node.getParent() instanceof ASTNodeListInterface)){
			throw new JastAddTreeEditException("Element " + node +  " not in list in: getListNameFor 1 of type" + node.getParent());
		}
		ASTNodeInterface parent = node.getParent();
		ASTNodeInterface grandParent = parent.getParent();
		String[] lists = Extractor.getList(grandParent.getClass());

		String res = null;
		for (int i = 0; i < lists.length; i++) {
			String name = lists[i];
			if(parent == extractItems(grandParent, name)){
				res = name;
			}
		}
		if(res == null){
			throw new JastAddTreeEditException("Element " + node +  " not in list in: getListNameFor 2");
		}
		return res;
	}
	public static String getOptNameFor(ASTNodeInterface node) {
		if(!(node.getParent() instanceof ASTNodeOptInterface)){
			throw new JastAddTreeEditException("Element " + node +  " not in opt in: getOptNameFor");
		}
		ASTNodeInterface parent = node.getParent();
		ASTNodeInterface grandParent = parent.getParent();
		String[] opts = Extractor.getOpt(grandParent.getClass());

		String res = null;
		for (int i = 0; i < opts.length; i++) {
			String name = opts[i];
			if(parent == extractItemOpt(grandParent, name)){
				res = name;
			}
		}
		
		if(res == null){
			throw new JastAddTreeEditException("Element " + node +  " not in list in: getOptNameFor");
		}
		return res;
	}
	
	public static String getShowNameFor(ASTNodeInterface node) {
		String res = getNameFor(node);
		
		String end;
		if(node instanceof ASTNodeListInterface){
			end = "*";
			res = res.substring(0,res.length() - 4);
		} else if (node instanceof ASTNodeOptInterface){
			end = "?";
			res = res.substring(0,res.length() - 3);
		} else if (! res.equals(node.getClass().getSimpleName()) ){
			end = ":" + node.getClass().getSimpleName();
		} else {
			end = "";
		}
		return res + end;
	}

	public static String getNameFor(ASTNodeInterface node) {
		ASTNodeInterface parent = node.getParent();
		if(parent == null){
			return node.getClass().getSimpleName(); 
		}
		String[] children = Extractor.getAll(parent.getClass());
		String res = null;
		for (int i = 0; i < children.length; i++) {
			String name = children[i];
			if(node == extractItem(parent, name)){
				res = name;
			}
		}
		if(res == null){
			for(String s : children){
		//		System.out.println(s);
			}
			res = "Fix this later";
		//	throw new JastAddTreeEditException("Can node get name for: " + node.getClass().getSimpleName());
		}
		return res;
	}
	
	public static int getListIndex(ASTNodeInterface node ) {
		ASTNodeInterface list = node.getParent();
		int res = -1;
		for(int i = 0; i < list.getNumChild(); i++){
			if(list.getChild(i) == node){
				res = i;
			}
		}	
		return res;
	}


	public static void changeElement(ASTNodeInterface oldNode, Class<?> toCreate , SubclassExtractor se) {
		ASTNodeInterface parent = oldNode.getParent();
		int index = getListIndex(oldNode);
		ASTNodeInterface node;
		
		
		try {
			 
			try {
				Method method = toCreate.getMethod("ED_CREATE");
				node = (ASTNodeInterface) method.invoke(null);
			} catch (Exception e1) {
				node = null;
			}

			if(node == null){
				node = (ASTNodeInterface) toCreate.newInstance();
				se.inferAll(node);
			}
			
			Class<?> oldNodeClass = oldNode.getClass();
			String[] oldMethod = Extractor.getAll(oldNodeClass);
			for(String s : oldMethod){
				try {
					Method oldGet = oldNodeClass.getMethod("get" + s);
					Class<?> type = oldGet.getReturnType();
					Method newSet = toCreate.getMethod("set" + s, type);
					newSet.invoke(node, oldGet.invoke(oldNode));
				} catch (NoSuchMethodException | SecurityException | IllegalArgumentException | InvocationTargetException e) {
					//Nothing to worry about.
				}
				
			}
			oldMethod = Extractor.getTokens(oldNodeClass);
			for(String s : oldMethod){
				try {
					Method oldGet = oldNodeClass.getMethod("get" + s);
					Class<?> type = oldGet.getReturnType();
					Method newSet = toCreate.getMethod("set" + s, type);
					newSet.invoke(node, oldGet.invoke(oldNode));
				} catch (NoSuchMethodException | SecurityException | IllegalArgumentException | InvocationTargetException e) {
					//Nothing to worry about.
				}
				
			}
			
			parent.setChild(node, index);
		} catch (InstantiationException | IllegalAccessException e) {
			throw new JastAddTreeEditException("Error in changeching node: " + oldNode + " to node of type " + toCreate.getName(), e);
		}
		
	}
	
	public static void changeNode(ASTNodeInterface oldNode, ASTNodeInterface newNode ) {
		ASTNodeInterface parent = oldNode.getParent();
		int index = getListIndex(oldNode);
		parent.setChild(newNode, index);
	}
	
	public static void setElement(int index,ASTNodeInterface node, Class<?> toCreate , SubclassExtractor se) {
		try {
			ASTNodeInterface toInsert = (ASTNodeInterface) toCreate.newInstance();
			se.inferAll(toInsert);
			node.setChild(toInsert, index);
		} catch (InstantiationException | IllegalAccessException e) {
			throw new RuntimeException("Error in inserting value", e);
		}
		
	
	}
}
