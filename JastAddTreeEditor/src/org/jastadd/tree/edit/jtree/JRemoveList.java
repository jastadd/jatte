package org.jastadd.tree.edit.jtree;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenuItem;

import org.jastadd.tree.edit.ASTNodeInterface;
import org.jastadd.tree.edit.ASTNodeListInterface;
import org.jastadd.tree.edit.CompilerContainer;



public class JRemoveList extends JMenuItem implements ActionListener{

	private CompilerContainer cc;
	private ASTNodeInterface ni;
	private ASTNodeListInterface list;

	public JRemoveList(CompilerContainer cc, ASTNodeInterface ni, ASTNodeListInterface list) {
		super("Remove");
		this.cc = cc;
		this.ni = ni;
		this.list = list;
		addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		int res = -1;
		for(int i = 0; i < list.getNumChild(); i++){
			if(list.getChild(i) == ni){
				res = i;
			}
		}
		list.removeChild(res);
		cc.addChangedNode(list);
		cc.hasChangeTree();
	}

}
