package org.jastadd.tree.edit.jtree;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.Observable;
import java.util.Observer;

import javax.swing.DefaultCellEditor;
import javax.swing.DropMode;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.TransferHandler;
import javax.swing.UIManager;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeExpansionListener;
import javax.swing.event.TreeWillExpandListener;
import javax.swing.plaf.TreeUI;
import javax.swing.plaf.basic.BasicTreeUI;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.ExpandVetoException;
import javax.swing.tree.TreeCellRenderer;
import javax.swing.tree.TreePath;

import org.jastadd.tree.edit.ASTNodeInterface;
import org.jastadd.tree.edit.CompilerContainer;
import org.jastadd.tree.edit.JatteCellEditor;
import org.jastadd.tree.edit.exception.JastAddTreeEditException;
import org.jastadd.tree.edit.xml.JastAddXMLParser;

import internal.org.xmlpull.v1.XmlPullParserException;

public class MainJtreeContatiner extends MainContainer {

	private static final long serialVersionUID = -66254797654760275L;
	private JTree jtree;
	private ASTTreeModel jtreeModel;
	
	private LinkedList<ASTNodeInterface> history = new LinkedList<>();

	public MainJtreeContatiner(byte[] data, CompilerContainer cc, String pakage) {
		super(data,cc,pakage);
		this.jtreeModel = new ASTTreeModel(astNode);
		this.jtree = new JTree(jtreeModel);
		MouseListener ml = new WhenMousePress();
		jtree.addMouseListener(ml);
		
		jtree.addTreeWillExpandListener(new TreeWillExpandListener() {
			@Override
			public void treeWillExpand(TreeExpansionEvent event) throws ExpandVetoException {
				
			}
			@Override
			public void treeWillCollapse(TreeExpansionEvent event) throws ExpandVetoException {
				throw new ExpandVetoException(event);
			}
		});
		
		for (int i = 0; i < jtree.getRowCount(); i++) {
		    jtree.expandRow(i);
		}

		jtree.setDropMode(DropMode.ON);
		jtree.setDragEnabled(true);
		jtree.setTransferHandler(new JastAddTreeEditorTransferHandler(cc));
	//	DefaultTreeCellRenderer renderer = (DefaultTreeCellRenderer) jtree.getCellRenderer();

	//	Icon icon = new ImageIcon(Square.data());
	//	renderer.setClosedIcon(icon);
	//	renderer.setOpenIcon(icon);
	//	renderer.setLeafIcon(icon);  
		
	//	jtree.setCellRenderer(new TreeCellRenderer() {
	//		@Override
	//		public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded,
	//				boolean leaf, int row, boolean hasFocus) {
	//			ASTNodeInterface ast = (ASTNodeInterface) value;
	//			JTextField hello = new JTextField(ast.ed_label());
	//			return hello;
	//		}
	//	});
//		JTextField test = new JTextField("hej");
//		test.addActionListener(new ActionListener() {
//			
//			@Override
//			public void actionPerformed(ActionEvent e) {
//				System.out.println("hello");
//				
//			}
//		});
		JatteCellEditor jce = new JatteCellEditor();
		jtree.setCellEditor(jce);
		jtree.setCellRenderer(new TreeCellRenderer() {
			@Override
			public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded,
					boolean leaf, int row, boolean hasFocus) {
				return jce.getTreeCellEditorComponent(tree, value, selected, expanded, leaf, row);
			}
		});
		Icon empty = new TreeIcon();
		TreeUI tui = jtree.getUI();
		if (tui instanceof BasicTreeUI) {
			  ((BasicTreeUI)tui).setCollapsedIcon(empty);
			  ((BasicTreeUI)tui).setExpandedIcon(empty);
		}
		jtree.setEditable(true);
//		jtree.setRowHeight(40);

		setLayout(new BorderLayout());

		add(new JScrollPane(jtree), BorderLayout.CENTER);
		cc.addObserver(new CCObserver());
	}
	
	private class CCObserver implements Observer {
		@Override
		public void update(Observable o, Object arg) {
			MainJtreeContatiner.this.astNode.flushTreeCache();
		//	for (ASTNodeInterface node : cc.getChangesNodes()) {
		//		if (node.ed_show()) {
		//			jtreeModel.nodeChanges(node);
		//		} else {
		//			jtreeModel.nodeChanges(node.ed_show_parent());
		//		}
		//	}
			jtreeModel.nodeChanges(astNode);
			
			for (int i = 0; i < jtree.getRowCount(); i++) {
				jtree.expandRow(i);
			}
			jtree.revalidate();
			jtree.repaint();
			
			
		}
	}

	private final class WhenMousePress extends MouseAdapter {
		public void mousePressed(MouseEvent e) {
			int selRow = jtree.getRowForLocation(e.getX(), e.getY());
			TreePath selPath = jtree.getPathForLocation(e.getX(), e.getY());
			if (selRow != -1) {
				jtree.setSelectionPath(selPath);
				if (e.isPopupTrigger()) {
					ASTNodeInterface nodeAST = (ASTNodeInterface) selPath.getLastPathComponent();
					nodeAST.ed_right_click(cc, e);
				} else {

				}
			}
		}
	}
	
	class TreeIcon implements Icon {

	    private static final int SIZE = 0;

	    public TreeIcon() {
	    }

	    public int getIconWidth() {
	        return SIZE;
	    }

	    public int getIconHeight() {
	        return SIZE;
	    }


		@Override
		public void paintIcon(Component c, Graphics g, int x, int y) {
	        //System.out.println(c.getWidth() + " " + c.getHeight() + " " + x + " " + y);
		}
	}
}
