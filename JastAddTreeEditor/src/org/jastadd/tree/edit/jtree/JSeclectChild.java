package org.jastadd.tree.edit.jtree;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenu;
import javax.swing.JMenuItem;

import org.jastadd.tree.edit.ASTNodeInterface;
import org.jastadd.tree.edit.CompilerContainer;
import org.jastadd.tree.edit.Editors;




public class JSeclectChild extends JMenu {
	CompilerContainer cc;
	ASTNodeInterface ni;
	private String childName;

	public JSeclectChild(CompilerContainer cc, ASTNodeInterface ni, String child) {
		super(child);
		this.cc = cc;
		this.ni = ni;
		this.childName = child;
		Class<?>[] classes = Editors.getClasses(ni, childName, cc.getSubclassExtractor());
		for (Class<?> cl : classes) {
			add(new SelectChildMenuItem(cl));
		}
	}

	private class SelectChildMenuItem extends JMenuItem implements ActionListener {
		Class<?> cl;

		public SelectChildMenuItem(Class<?> cl) {
			super(cl.getSimpleName());
			this.cl = cl;
			addActionListener(this);
		}
		@Override
		public void actionPerformed(ActionEvent e) {
			Editors.editChild(ni, childName, cl, cc.getSubclassExtractor());
			cc.addChangedNode(ni);
			cc.hasChangeTree();
		}
	}
}
