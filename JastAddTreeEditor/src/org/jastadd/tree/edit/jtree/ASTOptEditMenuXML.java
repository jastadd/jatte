package org.jastadd.tree.edit.jtree;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.MouseEvent;

import javax.swing.JPopupMenu;

import org.jastadd.tree.edit.ASTNodeInterface;
import org.jastadd.tree.edit.CompilerContainer;
import org.jastadd.tree.edit.Editors;


public class ASTOptEditMenuXML extends JPopupMenu {

	ASTNodeInterface node;
	MouseEvent me;
	private CompilerContainer cc;

	public ASTOptEditMenuXML(CompilerContainer cc, ASTNodeInterface node, MouseEvent me) {
		super("Edit ni");
		this.node = node;
		this.me = me;
		this.cc = cc;
		setFont(new Font("Monospaced", Font.PLAIN, 14));
		
		String listName = Editors.getNameFor(node);

		add(new JSeclectOps(cc, node.getParent(), listName.substring(0, listName.length() - 3)));

		JChangeChild jcl = new JChangeChild(cc, node);
		if (jcl.getItemCount() > 1) {
			add(jcl);
		}

		show(me.getComponent(), me.getX(), me.getY());
	}

}
