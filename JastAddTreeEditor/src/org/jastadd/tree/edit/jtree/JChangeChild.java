package org.jastadd.tree.edit.jtree;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenu;
import javax.swing.JMenuItem;

import org.jastadd.tree.edit.ASTNodeInterface;
import org.jastadd.tree.edit.CompilerContainer;
import org.jastadd.tree.edit.Editors;



public class JChangeChild extends JMenu {

	private CompilerContainer cc;
	private ASTNodeInterface ni;
	private String name;

	public JChangeChild(CompilerContainer cc, ASTNodeInterface ni) {
		super("Change");
		this.cc = cc;
		this.ni = ni;
		this.name = Editors.getNameFor(ni);
		
		Class<?>[] classes = Editors.getClasses(ni.getParent(), name, cc.getSubclassExtractor());
		for (Class<?> cl : classes) {
			add(new SelectListMenuItem(cl));
		}
	}
	
	private class SelectListMenuItem extends JMenuItem implements ActionListener {
		Class<?> cl;

		public SelectListMenuItem(Class<?> cl) {
			super(cl.getSimpleName());
			this.cl = cl;
			addActionListener(this);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			Editors.changeElement(ni, cl, cc.getSubclassExtractor());
			cc.addChangedNode(ni.ed_show_parent());
			cc.hasChangeTree();
		}

	}
}
