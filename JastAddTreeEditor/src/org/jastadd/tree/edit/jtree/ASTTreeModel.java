package org.jastadd.tree.edit.jtree;

import java.util.LinkedList;
import java.util.List;

import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

import org.jastadd.tree.edit.ASTNodeInterface;

public class ASTTreeModel implements TreeModel {
	
	private Object root;
	
	private List<TreeModelListener> listeners= new LinkedList<>();

	public ASTTreeModel(ASTNodeInterface root) {
		this.root = root;
	}

	@Override
	public Object getRoot() {
		return root;
	}

	@Override
	public Object getChild(Object parent, int index) {
		if(parent instanceof ASTNodeInterface){
			ASTNodeInterface astParent = (ASTNodeInterface) parent;
			return astParent.ed_children_to_show().get(index);
		}
		return null;
	}

	@Override
	public int getChildCount(Object parent) {
		if(parent instanceof ASTNodeInterface){
			ASTNodeInterface astParent = (ASTNodeInterface) parent;
			return astParent.ed_children_to_show().size();
		}
		return 0;
	}

	@Override
	public boolean isLeaf(Object node) {
		if(node instanceof ASTNodeInterface){
			ASTNodeInterface astParent = (ASTNodeInterface) node;
			return astParent.ed_children_to_show().isEmpty();
		}
		return false;
	}

	@Override
	public void valueForPathChanged(TreePath path, Object newValue) {
		

	}

	@Override
	public int getIndexOfChild(Object parent, Object child) {
		if(parent instanceof ASTNodeInterface){
			ASTNodeInterface astParent = (ASTNodeInterface) parent;
			return astParent.ed_children_to_show().indexOf(child);
		}
		return 0;
	}

	@Override
	public void addTreeModelListener(TreeModelListener l) {
		listeners.add(l);
	}

	@Override
	public void removeTreeModelListener(TreeModelListener l) {
		listeners.remove(l);
	}
	
	public void nodeChanges(ASTNodeInterface node){
		LinkedList<ASTNodeInterface> nodes = new LinkedList();
		ASTNodeInterface tmp = node;
		ASTNodeInterface last = null;
		while(tmp != last){
			nodes.push(tmp);
			last = tmp;
			tmp = tmp.ed_show_parent();
		}
		
		for(TreeModelListener listener : listeners){
			listener.treeStructureChanged(new TreeModelEvent(node, nodes.toArray()));
		}
		
	}

}
