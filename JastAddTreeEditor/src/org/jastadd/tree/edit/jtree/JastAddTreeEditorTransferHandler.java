package org.jastadd.tree.edit.jtree;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.InputEvent;
import java.io.IOException;


import javax.swing.JComponent;
import javax.swing.JTree;
import javax.swing.TransferHandler;
import javax.swing.tree.TreePath;

import org.jastadd.tree.edit.ASTNodeInterface;
import org.jastadd.tree.edit.CompilerContainer;
import org.jastadd.tree.edit.exception.JastAddTreeEditException;


public class JastAddTreeEditorTransferHandler extends TransferHandler {

	private static final long serialVersionUID = 7023398261787444446L;

	private CompilerContainer cc;

	public JastAddTreeEditorTransferHandler(CompilerContainer cc) {
		super();
		this.cc = cc;
	}

	@Override
	public boolean canImport(TransferSupport support) {
		if (!support.isDrop()) {
			return false;
		}
		support.setShowDropLocation(true);
		try {
			JTree.DropLocation dl = (javax.swing.JTree.DropLocation) support.getDropLocation();
			if(dl == null || dl.getPath() == null){
				return false;
			}
			ASTNodeInterface node = (ASTNodeInterface) dl.getPath().getLastPathComponent();
//			if (node.dataFlavorType() == null) {
//				return false;
//			}
//			for (Class c : node.dataFlavorType()) {
//				if (support.isDataFlavorSupported(new DataFlavor(c, "PON")) || support.isDataFlavorSupported(new DataFlavor(c, "Resource"))) {
//					return true;
//				}
//			} 
			for(DataFlavor df : support.getDataFlavors()){
				if(node.ed_can_drop(support.getTransferable().getTransferData(df))){
					return true;
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean importData(TransferSupport support) {
		try {
			JTree.DropLocation dl = (javax.swing.JTree.DropLocation) support.getDropLocation();
			ASTNodeInterface node = (ASTNodeInterface) dl.getPath().getLastPathComponent();
//			Class cls = null;
//			for (Class c : node.dataFlavorType()) {
//				if (support.isDataFlavorSupported(new DataFlavor(c, "PON")) || support.isDataFlavorSupported(new DataFlavor(c, "Resource"))) {
//					cls = c;
//				}
//			}
			for(DataFlavor df : support.getDataFlavors()){
				Object data = support.getTransferable().getTransferData(df);
				if(node.ed_can_drop(data)){
					node.ed_accept_resource(data, cc);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new JastAddTreeEditException(e);
		}
		return true;
	}
	
	@Override
	public int getSourceActions(JComponent c) {
//		System.out.println(c);
		TreePath tp = ((JTree)c).getSelectionPath();
		if(tp == null){
			return NONE;
		}
		ASTNodeInterface n = (ASTNodeInterface) tp.getLastPathComponent();
		if(n.ed_can_drag()){
			return COPY;
		} else {
			return NONE;
		}
	}
	
	@Override
	protected Transferable createTransferable(JComponent c) {
		TreePath tp = ((JTree)c).getSelectionPath();
		ASTNodeInterface n = (ASTNodeInterface) tp.getLastPathComponent();
		return new Transferable() {
			@Override
			public boolean isDataFlavorSupported(DataFlavor flavor) {
				return flavor.getClass().equals(n.getClass());
			}
			
			@Override
			public DataFlavor[] getTransferDataFlavors() {
				return new DataFlavor[]{new DataFlavor(n.getClass(), "PON")};
			}
			
			@Override
			public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException, IOException {
				return n;
			}
		};
	} 
	
	@Override
	public void exportAsDrag(JComponent comp, InputEvent e, int action) {
//		System.out.println(comp);
		super.exportAsDrag(comp, e, action);
	}
}
