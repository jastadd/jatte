package org.jastadd.tree.edit.jtree;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenu;
import javax.swing.JMenuItem;

import org.jastadd.tree.edit.ASTNodeInterface;
import org.jastadd.tree.edit.ASTNodeListInterface;
import org.jastadd.tree.edit.CompilerContainer;
import org.jastadd.tree.edit.Editors;


public class JAddList extends JMenu {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	CompilerContainer cc;
	ASTNodeInterface ni;
	private String listName;

	public JAddList(CompilerContainer cc, ASTNodeInterface ni, String child) {
		super("Add:" + child);
		this.cc = cc;
		this.ni = ni;
		this.listName = child;
		

		Class<?>[] classes = Editors.getClassesList(ni, listName, cc.getSubclassExtractor());
		for (Class<?> cl : classes) {
			add(new SelectListMenuItem(cl));
		}

	}

	private class SelectListMenuItem extends JMenuItem implements ActionListener {
		Class<?> cl;

		public SelectListMenuItem(Class<?> cl) {
			super(cl.getSimpleName());
			this.cl = cl;
			addActionListener(this);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			ASTNodeListInterface list = Editors.extractItems(ni, listName);
			Editors.addToList(list, cl, cc.getSubclassExtractor());
			cc.addChangedNode(list);
			cc.hasChangeTree();
		}

	}
}
