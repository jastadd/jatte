package org.jastadd.tree.edit.jtree;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.MouseEvent;

import javax.swing.JPopupMenu;

import org.jastadd.tree.edit.ASTNodeInterface;
import org.jastadd.tree.edit.CompilerContainer;
import org.jastadd.tree.edit.MenuContainerInterface;
import org.jastadd.tree.edit.MenuInterface;

public class ASTNodeShowMenu  extends JPopupMenu{
	
	ASTNodeInterface node;
	MouseEvent me;
	private CompilerContainer cc;
	
	public ASTNodeShowMenu(CompilerContainer cc, ASTNodeInterface node, MouseEvent me){
		
		setFont(new Font("Monospaced", Font.PLAIN, 14));

		MenuContainerInterface oprions = node.ed_menu(cc);
		for(MenuInterface row : oprions.getMenuList().getMenus()){
			add(row.ed_generate_menu(cc));
		}
		show(me.getComponent(), me.getX(), me.getY());
	}
	

}
