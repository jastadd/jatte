package org.jastadd.tree.edit.jtree;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTextField;

import org.jastadd.tree.edit.ASTNodeInterface;
import org.jastadd.tree.edit.CompilerContainer;
import org.jastadd.tree.edit.Editors;


public class JChangeToken extends JTextField implements ActionListener{
	

	private CompilerContainer cc;
	private ASTNodeInterface node;
	private String token;

	public JChangeToken(ASTNodeInterface node, String token,CompilerContainer cc) {
		super(Editors.getValue(node, token));
		this.cc = cc;
		this.node = node;
		this.token = token;
		
		addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Editors.setValue(token, getText(), node);
		cc.addChangedNode(node);
		cc.hasChangeTree();
	}
	
	

}
