package org.jastadd.tree.edit.jtree;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenuItem;

import org.jastadd.tree.edit.ASTNodeInterface;
import org.jastadd.tree.edit.ASTNodeOptInterface;
import org.jastadd.tree.edit.CompilerContainer;


public class JRemoveOpt extends JMenuItem implements ActionListener{

	private CompilerContainer cc;
	private ASTNodeInterface ni;
	private ASTNodeOptInterface opt;

	public JRemoveOpt(CompilerContainer cc, ASTNodeInterface ni, ASTNodeOptInterface opt) {
		super("Remove");
		this.cc = cc;
		this.ni = ni;
		this.opt = opt;
		addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		opt.removeChild(0);
		cc.addChangedNode(opt);
		cc.hasChangeTree();
	}

}
