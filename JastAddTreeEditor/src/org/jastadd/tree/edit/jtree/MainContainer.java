package org.jastadd.tree.edit.jtree;

import java.awt.LayoutManager;
import java.io.IOException;

import javax.swing.JPanel;

import org.jastadd.tree.edit.ASTNodeInterface;
import org.jastadd.tree.edit.CompilerContainer;
import org.jastadd.tree.edit.exception.JastAddTreeEditException;
import org.jastadd.tree.edit.xml.JastAddXMLParser;
import org.jastadd.tree.edit.xml.JastAddXMLSerializer;

import internal.org.xmlpull.v1.XmlPullParserException;

public abstract class MainContainer extends JPanel {

	protected ASTNodeInterface astNode;
	protected CompilerContainer cc;
	protected String pakage;
	
	
	public MainContainer(byte[] data, CompilerContainer cc, String pakage) {
		this.cc = cc;
		this.pakage = pakage;
		try {
			this.astNode = (ASTNodeInterface) JastAddXMLParser.parse(new String(data), pakage);
		} catch (XmlPullParserException e1) {
			throw new JastAddTreeEditException("XML Error", e1);
		} catch (IOException e1) {
			throw new JastAddTreeEditException("Io Error", e1);
		}
		
	}
	

	public MainContainer(ASTNodeInterface data, CompilerContainer cc, String pakage) {
		this.cc = cc;
		this.pakage = pakage;
		this.astNode = data;
	}


	public ASTNodeInterface getAstNode() {
		return astNode;
	}

	public byte[] getData() {
		try {
			return JastAddXMLSerializer.serializ(astNode).getBytes();
		} catch (IOException e) {
			throw new JastAddTreeEditException("Io Error", e);
		}
	}

}