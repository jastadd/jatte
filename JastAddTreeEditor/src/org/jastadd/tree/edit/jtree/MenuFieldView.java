package org.jastadd.tree.edit.jtree;

import java.awt.BorderLayout;
import java.awt.event.ActionListener;
import java.awt.Color;
import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class MenuFieldView extends JPanel {

	private JTextField input;

	public MenuFieldView(String name, String value){
		
		setLayout(new BorderLayout());
		setFont(new Font("Monospaced", Font.PLAIN, 14));
		
		JLabel label = new JLabel(name);
		
		label.setFont(new Font("Monospaced", Font.PLAIN, 14));
		input = new JTextField(value);
		input.setFont(new Font("Monospaced", Font.PLAIN, 14));
		input.setColumns(Math.min(30, value.length()));
		
		add(label,BorderLayout.WEST);
		add(input,BorderLayout.CENTER);
		
	}
	
	public void addActionListnerToInput(ActionListener actionListner){
		input.addActionListener(actionListner);		
	}

	public String getInputValue() {
		return input.getText();
	}
}
