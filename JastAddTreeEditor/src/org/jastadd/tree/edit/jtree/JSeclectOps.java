package org.jastadd.tree.edit.jtree;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenu;
import javax.swing.JMenuItem;

import org.jastadd.tree.edit.ASTNodeInterface;
import org.jastadd.tree.edit.CompilerContainer;
import org.jastadd.tree.edit.Editors;



public class JSeclectOps extends JMenu {
	CompilerContainer cc;
	ASTNodeInterface ni;
	private String opsName;

	public JSeclectOps(CompilerContainer cc, ASTNodeInterface ni, String child) {
		super(child);
		this.cc = cc;
		this.ni = ni;
		this.opsName = child;
		
		add(new SelectNoneMenuItem());

		Class<?>[] classes = Editors.getClasses(ni, opsName, cc.getSubclassExtractor());
		for (Class<?> cl : classes) {
			add(new SelectOpsMenuItem(cl));
		}

	}

	private class SelectOpsMenuItem extends JMenuItem implements ActionListener {
		Class<?> cl;

		public SelectOpsMenuItem(Class<?> cl) {
			super(cl.getSimpleName());
			this.cl = cl;
			addActionListener(this);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			Editors.addOpt(ni, opsName, cl, cc.getSubclassExtractor());
			cc.addChangedNode(ni);
			cc.hasChangeTree();
		}

	}
	
	private class SelectNoneMenuItem extends JMenuItem implements ActionListener {

		public SelectNoneMenuItem() {
			super("None");
			addActionListener(this);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			Editors.removeOpt(ni, opsName);
			cc.addChangedNode(ni);
			cc.hasChangeTree();
		}

	}

}
