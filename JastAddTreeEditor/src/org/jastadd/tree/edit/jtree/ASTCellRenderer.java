package org.jastadd.tree.edit.jtree;

import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JTree;
import javax.swing.tree.TreeCellRenderer;

import org.jastadd.tree.edit.ASTNodeInterface;
import org.jastadd.tree.edit.ASTNodeListInterface;
import org.jastadd.tree.edit.ASTNodeOptInterface;
import org.jastadd.tree.edit.Editors;
import org.jastadd.tree.edit.Extractor;


//public class ASTCellRenderer implements TreeCellRenderer {
	
//	//Not in use !!!!!!!!!!!!!!!!!!!!!
//
//	@Override
//	public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded,
//			boolean leaf, int row, boolean hasFocus) {
//		
//		String res = "";
//
//		ASTNodeInterface node = (ASTNodeInterface) value;
//		if (node.ed_label() == null){
//			res = getDefultString(res, node);
//		} else {
//			res = node.ed_label();
//		}
//		JLabel out = new JLabel(res);
//		
//		
//		return out;
//	}
//
//	String getDefultString(String res, ASTNodeInterface node) {
//		if (node.getParent() == null) {
//			res += "root";
//		} else if (node.getParent() instanceof ASTNodeListInterface || node.getParent() instanceof ASTNodeOptInterface) {
//			res += node.getClass().getSimpleName();
//		} else {
//			res += Editors.getShowNameFor(node);
//		}
//		
//		for(String token : Extractor.getTokens(node.getClass())){
//			String tokenValue = Editors.getValue(node, token);
//			res += ", " + token + ": " + tokenValue; 
//		}
//		return res;
//	}

//}
