package org.jastadd.tree.edit.jtree;

import java.awt.event.MouseEvent;

import javax.swing.JPopupMenu;

import org.jastadd.tree.edit.ASTNodeInterface;
import org.jastadd.tree.edit.ASTNodeListInterface;
import org.jastadd.tree.edit.ASTNodeOptInterface;
import org.jastadd.tree.edit.CompilerContainer;
import org.jastadd.tree.edit.Editors;
import org.jastadd.tree.edit.Extractor;


public class ASTNodeInterfaceEditMenuXML extends JPopupMenu {

	ASTNodeInterface node;
	MouseEvent me;
	private CompilerContainer cc;

	public ASTNodeInterfaceEditMenuXML(CompilerContainer cc, ASTNodeInterface node, MouseEvent me) {
		super("Edit ni");
		this.node = node;
		this.me = me;
		this.cc = cc;
		//add(new JMenuItem("Edit: " + node.getClass().getSimpleName()));

		for (String child : Extractor.getChild(node.getClass())) {
			Class<?>[] classes = Editors.getClasses(node, child, cc.getSubclassExtractor());
			if (classes.length > 1) {
				add(new JSeclectChild(cc, node, child));
			}
		}

		for (String opt : Extractor.getOpt(node.getClass())) {
			add(new JSeclectOps(cc, node, opt));
		}

		for (String list : Extractor.getList(node.getClass())) {
			add(new JAddList(cc, node, list));
		}

		if (node.getParent() instanceof ASTNodeListInterface) {
			ASTNodeListInterface list = (ASTNodeListInterface) node.getParent();
			add(new JRemoveList(cc, node, list));
			JChangeList jcl = new JChangeList(cc, node);
			if (jcl.getItemCount() > 1) {
				add(jcl);
			}
		} else if (node.getParent() instanceof ASTNodeOptInterface) {
			ASTNodeOptInterface	list = (ASTNodeOptInterface) node.getParent();
			add(new JRemoveOpt(cc, node, list));
			// add(new JChangeOpt(cc,ni,list)); // TO DO
		} else {
			JChangeChild jcl = new JChangeChild(cc, node);
			if (jcl.getItemCount() > 1) {
				add(jcl);
			}
		}
		
		for (String token: Extractor.getTokens(node.getClass())) {
			add(new JChangeToken(node,token,cc));
		}

		show(me.getComponent(), me.getX(), me.getY());
	}

}
