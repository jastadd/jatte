package org.jastadd.tree.edit.jtree;

public class Square {
	
	private static String box = "89504e470d0a1a0a0000000d494844520000001000000010080200000090916836000000097048597300000b1300000b1301009a9c180000000774494d4507e1060d07091f3563efc30000001d69545874436f6d6d656e7400000000004372656174656420776974682047494d50642e6507000000644944415428cf63fcffff3f032980898144407b0d2cc89c0b172e5cbc78114d85bebebe818101760d172f5e9c3061c2870f1fe02202020205050538353030307cf8f0e1c1830770ae8282c2100b25882f91dd2d202080a6801139691013ac8cc3202d0100d13124b0e5d4def40000000049454e44ae426082";
			
	public static byte[] data() {
	    int len = box.length();
	    byte[] data = new byte[len / 2];
	    for (int i = 0; i < len; i += 2) {
	        data[i / 2] = (byte) ((Character.digit(box.charAt(i), 16) << 4)
	                             + Character.digit(box.charAt(i+1), 16));
	    }
	    return data;
	}
			

}
