package org.jastadd.tree.edit;

import java.util.ArrayList;
import java.util.List;

import org.jastadd.tree.edit.xml.ASTConstructorAnnotation;
import org.jastadd.tree.edit.xml.ChildConstructorAnnotation;
import org.jastadd.tree.edit.xml.ListConstructorAnnotation;
import org.jastadd.tree.edit.xml.OptConstructorAnnotation;
import org.jastadd.tree.edit.xml.TokenConstructorAnnotation;


public class Extractor {
	

	public static String[] getTokens(Class<?> class1) {
		List<TokenConstructorAnnotation> tmp = ASTConstructorAnnotation.getTokenConstructorAnnotation(class1);
		String[] res = new String[tmp.size()];
		for (int i = 0; i < res.length; i++) {
			res[i] = tmp.get(i).name();
			
		}
		return res;
	}

	public static String[] getList(Class<?> class1) {
		List<ListConstructorAnnotation> tmp = ASTConstructorAnnotation.getListConstructorAnnotation(class1);
		String[] res = new String[tmp.size()];
		for (int i = 0; i < res.length; i++) {
			res[i] = tmp.get(i).name();
		}
		return res;
	}

	public static String[] getChild(Class<?> class1) {
		List<ChildConstructorAnnotation> tmp = ASTConstructorAnnotation.getChildConstructorAnnotation(class1);
		String[] res = new String[tmp.size()];
		for (int i = 0; i < res.length; i++) {
			res[i] = tmp.get(i).name();
		}
		return res;
	}

	public static String[] getOpt(Class<?> class1) {
		List<OptConstructorAnnotation> tmp = ASTConstructorAnnotation.getOptConstructorAnnotation(class1);
		String[] res = new String[tmp.size()];
		for (int i = 0; i < res.length; i++) {
			res[i] = tmp.get(i).name();
		}
		return res;
	}
	public static String[] getAll(Class<?> class1) {
		List<ASTConstructorAnnotation> tmp = ASTConstructorAnnotation.getConstructorParameters(class1);
		ArrayList<String> res = new ArrayList<>();
		for (ASTConstructorAnnotation t : tmp) {
			if(t instanceof ChildConstructorAnnotation){
				res.add(t.name());
			} else if(t instanceof OptConstructorAnnotation){
				res.add(t.name() + "Opt");
			} else if(t instanceof ListConstructorAnnotation){
				res.add(t.name() + "List");
			}
		}
		return res.toArray(new String[0]);
	}
	
	
	
	

}
