package org.jastadd.tree.edit;

import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractCellEditor;
import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.tree.TreeCellEditor;

public class JatteCellEditor extends AbstractCellEditor implements TreeCellEditor {

	@Override
	public Object getCellEditorValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Component getTreeCellEditorComponent(JTree tree, Object value, boolean isSelected, boolean expanded,
			boolean leaf, int row) {
		ASTNodeInterface astNode = (ASTNodeInterface) value;
//		JPanel jpanel = new JPanel();
//		FlowLayout fl = new FlowLayout();
//		fl.setHgap(0);
//		fl.setVgap(0);
//		jpanel.setLayout(fl);
//		
//		JTextField tmp1 = new JTextField("tmp2");
//		tmp1.setMargin(new Insets(0, 0, 0, 0));
//		tmp1.setBorder(BorderFactory.createLineBorder(Color.BLACK, 1));
//		jpanel.setBackground(Color.YELLOW);
//		tmp1.addActionListener(new ActionListener() {
//			@Override
//			public void actionPerformed(ActionEvent e) {
//				System.out.println("Actionlistner111");
//			}
//		});
//		
//		JTextField tmp2 = new JTextField("tmp1");
//		tmp2.setMargin(new Insets(0, 0, 0, 0));
//		tmp2.setBorder(BorderFactory.createLineBorder(Color.BLACK, 1));
//		tmp2.addActionListener(new ActionListener() {
//			@Override
//			public void actionPerformed(ActionEvent e) {
//				System.out.println("Actionlistner222");
//			}
//		});
		//jpanel.add(tmp1);
		//jpanel.add(tmp2);
		JTree.DropLocation dropLocation = tree.getDropLocation();
		JLabel res = new JLabel(astNode.ed_label());
		if(isSelected){
			//res.setBackground(Color.YELLOW);
			//res.setBorder(BorderFactory.createLineBorder(Color.BLACK, 1));
			res.setFont(new Font("Monospaced", Font.BOLD, 12));
		} else if (dropLocation != null
	             && dropLocation.getChildIndex() == -1
	             && tree.getRowForPath(dropLocation.getPath()) == row) {
			res.setFont(new Font("Monospaced", Font.BOLD, 12));
	     }
		else{
			res.setFont(new Font("Monospaced", Font.PLAIN, 12));
		}
//		res.setTransferHandler(new );
		//jpanel.add(res);
//		JLabel res2 = new JLabel(astNode.ed_label());
//		res2.setFont(new Font("Monospaced", Font.PLAIN, 12));
		//jpanel.add(res2);
//		JComboBox<String> jcombo = new JComboBox<>();
//		jcombo.addItem("Hello");
//		jcombo.addItem("Hello1");
		//jcombo.setBorder(BorderFactory.createLineBorder(Color.BLACK, 1));
		//LayoutManager layout = jcombo.getLayout();
		//System.out.println(layout);
		
//		jpanel.add(jcombo);
		
		return res;
	}

}
