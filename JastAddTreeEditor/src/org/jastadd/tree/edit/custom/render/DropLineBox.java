package org.jastadd.tree.edit.custom.render;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.TooManyListenersException;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.TransferHandler;

import org.jastadd.tree.edit.ASTNodeInterface;
import org.jastadd.tree.edit.CompilerContainer;
import org.jastadd.tree.edit.DragListner;

public abstract class DropLineBox extends JPanel implements DragListner {
	private static Color lightColor = new Color(220, 220, 220);
	protected ASTNodeInterface node;
	private CompilerContainer cc;
	private DropLine dl;

	public DropLineBox(CompilerContainer cc, ASTNodeInterface node) {
		super();
		this.node = node;
		this.cc = cc;
		setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		setAlignmentX(Component.LEFT_ALIGNMENT);
		add(Box.createRigidArea(new Dimension(indent() * 20, 2))); // Replace
																	// with
																	// upper and
																	// lower
																	// indent
		dl = new DropLine();
		add(dl);
		setForeground(Color.white);
		setBackground(Color.white);
		cc.addDragListner(this);
	}

	abstract protected int indent();

	private class DropLine extends JPanel {

		public DropLine() {
			super();
			setAlignmentX(Component.LEFT_ALIGNMENT);
			setMinimumSize(new Dimension(2, 2));
			setMaximumSize(new Dimension(1000000, 2));
			setForeground(Color.white);
			setBackground(Color.white);

			setTransferHandler(new UnderLineTransferHandler());

			try {
				getDropTarget().addDropTargetListener(new DropTargetListener() {

					@Override
					public void dropActionChanged(DropTargetDragEvent dtde) {
						// TODO Auto-generated method stub

					}

					@Override
					public void drop(DropTargetDropEvent dtde) {
						// TODO Auto-generated method stub

					}

					@Override
					public void dragOver(DropTargetDragEvent dtde) {
						// TODO Auto-generated method stub

					}

					@Override
					public void dragExit(DropTargetEvent dte) {
						SwingUtilities.invokeLater(new Runnable() {
							@Override
							public void run() {
								if (canDrop(cc.currentDragging())) {
									setForeground(lightColor);
									setBackground(lightColor);
								}
							}
						});

					}

					@Override
					public void dragEnter(DropTargetDragEvent dtde) {
						SwingUtilities.invokeLater(new Runnable() {
							@Override
							public void run() {
								if (canDrop(cc.currentDragging())) {
									setForeground(Color.black);
									setBackground(Color.black);
								}
							}
						});
					}
				});
			} catch (TooManyListenersException e) {
				e.printStackTrace();
			}
		}

	}

	private class UnderLineTransferHandler extends TransferHandler {

		@Override
		public boolean canImport(TransferSupport support) {
			for (DataFlavor df : support.getDataFlavors()) {
				try {
					if (canDrop(support.getTransferable().getTransferData(df))) {
						return true;
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			return false;
		}

		@Override
		public boolean importData(TransferSupport support) {
			for (DataFlavor df : support.getDataFlavors()) {
				Object data;
				try {
					support.getTransferable();
					Transferable trns = support.getTransferable();
					data = trns.getTransferData(df);
					SwingUtilities.invokeLater(new Runnable() {

						@Override
						public void run() {
							if (canDrop(data)) {
								doDrop(data, cc);
							}
						}
					});
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			return true;
		}

	}

	volatile boolean stopedDrag = false;

	@Override
	public void begunDrag(Object node) {
		if (canDrop(node)) {
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					dl.setBackground(lightColor);
					dl.setForeground(lightColor);
				}
			});
		}
	}

	@Override
	public void endedDrag() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				dl.setBackground(Color.white);
				dl.setForeground(Color.white);
			}
		});
	}

	protected abstract boolean canDrop(Object o);

	protected abstract void doDrop(Object data, CompilerContainer cc);

	public static class UpperDropLineBox extends DropLineBox {

		public UpperDropLineBox(CompilerContainer cc, ASTNodeInterface node) {
			super(cc, node);
		}

		@Override
		protected boolean canDrop(Object o) {
			return node.ed_upper_can_drop(o);
		}

		@Override
		protected void doDrop(Object data, CompilerContainer cc) {
			node.ed_upper_accept_resource(data, cc);
		}

		@Override
		protected int indent() {
			return node.ed_upper_indent();
		}

	}

	public static class LowerDropLineBox extends DropLineBox {

		public LowerDropLineBox(CompilerContainer cc, ASTNodeInterface node) {
			super(cc, node);
		}

		@Override
		protected boolean canDrop(Object o) {
			return node.ed_lower_can_drop(o);
		}

		@Override
		protected void doDrop(Object data, CompilerContainer cc) {
			node.ed_lower_accept_resource(data, cc);

		}

		@Override
		protected int indent() {
			return node.ed_lower_indent();
		}

	}
}
