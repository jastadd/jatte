package org.jastadd.tree.edit.custom.render;

import java.awt.Color;
import java.awt.Component;

import javax.swing.BoxLayout;
import javax.swing.JPanel;

public class InnerBox extends JPanel {
	public InnerBox() {
		super();
		setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		setAlignmentX(Component.LEFT_ALIGNMENT);
		setBackground(new Color(255, 255, 255));
		
	}
	
	

}