package org.jastadd.tree.edit.custom.render;

import java.awt.Color;

import javax.swing.BoxLayout;
import javax.swing.JPanel;

public class OuterBox extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7104804834578815900L;

	public OuterBox() {
		super();
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
	}

}
