package org.jastadd.tree.edit.custom.render;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Deque;
import java.util.Observable;
import java.util.Observer;
import java.util.Stack;
import java.util.concurrent.LinkedBlockingDeque;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JViewport;
import javax.swing.SwingUtilities;

import org.jastadd.tree.edit.ASTNodeInterface;
import org.jastadd.tree.edit.CompilerContainer;
import org.jastadd.tree.edit.history.History;
import org.jastadd.tree.edit.jtree.MainContainer;
import org.jastadd.tree.edit.xml.JastAddXMLSerializer;

public class MainCustomContainer extends MainContainer implements Observer {

	private History<ASTNodeInterface> history;
	private JScrollPane scroll;
	private Point p;

	public MainCustomContainer(ASTNodeInterface data, CompilerContainer cc, String pakage) {
		super(data,cc,pakage);
		setLayout(new BorderLayout());
		render();
		cc.addObserver(this);
		history = new History<ASTNodeInterface>(astNode.treeCopy());
	}

	public MainCustomContainer(byte[] data, CompilerContainer cc, String pakage) {
		super(data, cc, pakage);
		setLayout(new BorderLayout());
		render();
		cc.addObserver(this);
		history = new History<ASTNodeInterface>(astNode.treeCopy());
	}
	
	
	

	public void render() {
		removeAll();
		cc.clearDragListners();
		OuterBox rows = new OuterBox();
		StringBuilder sb = new StringBuilder();
		cc.preRenderAction(astNode);
		int nbrDecimal = String.valueOf(astNode.ed_last_line_number()).length();
		addRows2(rows, sb, nbrDecimal,astNode);
		rows.setBackground(new Color(255, 255, 255));
		
		
		 scroll = new JScrollPane(rows);
		 if (p != null){
			 scroll.getViewport().setViewPosition(p);
		 }
		
		

		add(scroll, BorderLayout.CENTER);

		JButton undoButton = new JButton("Undo");
		
		undoButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				SwingUtilities.invokeLater(new Runnable() {

				@Override
					public void run() {
						p = scroll.getViewport().getViewPosition();
						astNode = history.getPrevious().treeCopy();
						astNode.flushTreeCache();
						render();
						revalidate();
						repaint();
					}
				});
			}
		});
		
		JPanel p = new JPanel();
		p.add(undoButton);
		JButton generateText = new JButton("text to clipbord");
		generateText.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				Toolkit.getDefaultToolkit().getSystemClipboard().setContents(new StringSelection(sb.toString()) , null);
			}
		});
		p.add(generateText);
		
		add(p, BorderLayout.SOUTH);
	}




	private void addRows(OuterBox rows, StringBuilder sb, int nbrDecimal) {
		ArrayList<? extends ASTNodeInterface> list = astNode.ed_visible_rows();
		for (ASTNodeInterface node : list) {
			InnerBox row = new InnerBox();
			row.add(new LineNumberLable(node,nbrDecimal));
			row.add(Box.createRigidArea(new Dimension(node.ed_indent() * 20 + 5, 0)));
			for (int i = 0 ; i < node.ed_indent(); i++){
				sb.append("  ");
			}
			RowLabel2 rowLabel = new RowLabel2(cc, node);
			sb.append(rowLabel.textRep());
			sb.append('\n');
			row.add(rowLabel);
			cc.addDragListner(rowLabel);
			rows.add(new DropLineBox.UpperDropLineBox(cc,node));
			rows.add(row);
			rows.add(new DropLineBox.LowerDropLineBox(cc,node));
		}
	}
	private void addRows2(OuterBox rows, StringBuilder sb, int nbrDecimal, ASTNodeInterface astNode) {
		rows.add(new DropLineBox.UpperDropLineBox(cc,astNode));
		if(astNode.ed_show()){
			InnerBox row = new InnerBox();
			row.add(new LineNumberLable(astNode,nbrDecimal));
			row.add(Box.createRigidArea(new Dimension(astNode.ed_indent() * 20 + 5, 0)));
			for (int i = 0 ; i < astNode.ed_indent(); i++){
				sb.append("  ");
			}
			RowLabel2 rowLabel = new RowLabel2(cc, astNode);
			sb.append(rowLabel.textRep());
			sb.append('\n');
			row.add(rowLabel);
			cc.addDragListner(rowLabel);
			rows.add(row);
			
		}
		ArrayList<? extends ASTNodeInterface> list = astNode.ed_children_to_show();
		for (ASTNodeInterface node : list) {
			addRows2(rows, sb, nbrDecimal, node);
		}
		rows.add(new DropLineBox.LowerDropLineBox(cc,astNode));
	}
	
	

	@Override
	public void update(Observable o, Object arg) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				p = scroll.getViewport().getViewPosition();
				astNode.flushTreeCache();
				astNode = astNode.treeCopy();
				astNode.flushTreeCache();
				render();
				revalidate();
				repaint();
				history.setCurrent(astNode.treeCopy());
			}
		});
	}
}
