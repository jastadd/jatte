//package org.jastadd.tree.edit.custom.render;
//
//import java.awt.Color;
//import java.awt.Font;
//import java.awt.datatransfer.DataFlavor;
//import java.awt.datatransfer.Transferable;
//import java.awt.datatransfer.UnsupportedFlavorException;
//import java.awt.dnd.DropTarget;
//import java.awt.dnd.DropTargetDragEvent;
//import java.awt.dnd.DropTargetDropEvent;
//import java.awt.dnd.DropTargetEvent;
//import java.awt.dnd.DropTargetListener;
//import java.awt.event.MouseEvent;
//import java.awt.event.MouseListener;
//import java.io.IOException;
//import java.util.TooManyListenersException;
//
//import javax.swing.BorderFactory;
//import javax.swing.JComponent;
//import javax.swing.JLabel;
//import javax.swing.JTextField;
//import javax.swing.SwingUtilities;
//import javax.swing.TransferHandler;
//
//import org.jastadd.tree.edit.ASTNodeInterface;
//import org.jastadd.tree.edit.CompilerContainer;
//import org.jastadd.tree.edit.DragListner;
//
//public class RowLabel extends JLabel implements DragListner {
//
//	CompilerContainer cc;
//	ASTNodeInterface node;
//
//	public RowLabel(CompilerContainer cc, ASTNodeInterface node) {
//		super();
//		this.cc = cc;
//		this.node = node;
//		setText(node.ed_label());
//		setFont(new Font("Monospaced", Font.PLAIN, 14));
//		TransferHandler tfh = new RowTransferHandler();
//		setTransferHandler(tfh);
//		try {
//			getDropTarget().addDropTargetListener(new DropTargetListener() {
//				
//				@Override
//				public void dropActionChanged(DropTargetDragEvent dtde) {
//				}
//				
//				@Override
//				public void drop(DropTargetDropEvent dtde) {
//				}
//				
//				@Override
//				public void dragOver(DropTargetDragEvent dtde) {
//				}
//				
//				@Override
//				public void dragExit(DropTargetEvent dte) {
//					if(node.ed_can_drop(cc.currentDragging())){
//						setBorder(BorderFactory.createLineBorder(new Color(101, 123, 131), 1));
//						setFont(new Font("Monospaced", Font.PLAIN, 14));
//					}
//				}
//				
//				@Override
//				public void dragEnter(DropTargetDragEvent dtde) {
//					if(node.ed_can_drop(cc.currentDragging())){
//						setBorder(BorderFactory.createLineBorder(new Color(101, 123, 131), 1));
//						setFont(new Font("Monospaced", Font.BOLD, 14));
//					}
//				}
//			});
//		} catch (TooManyListenersException e1) {
//			e1.printStackTrace();
//		}
//
//		addMouseListener(new MouseListener() {
//
//			@Override
//			public void mouseReleased(MouseEvent e) {
//				// TODO Auto-generated method stub
//			}
//
//			@Override
//			public void mousePressed(MouseEvent e) {
//				if (SwingUtilities.isRightMouseButton(e)) {
//					node.ed_right_click(cc, e);
//				} else if (SwingUtilities.isLeftMouseButton(e) && node.ed_can_drag()) {
//					tfh.exportAsDrag(RowLabel.this, e, TransferHandler.COPY);
//				}
//			}
//
//			@Override
//			public void mouseExited(MouseEvent e) {
//				if(cc.currentDragging() != node && !cc.dragging()){
//					setFont(new Font("Monospaced", Font.PLAIN, 14));
//					RowLabel.this.revalidate();
//				}
//			}
//
//			@Override
//			public void mouseEntered(MouseEvent e) {
//				if(!cc.dragging()){
//					setFont(new Font("Monospaced", Font.BOLD, 14));
//					RowLabel.this.revalidate();
//				}
//			}
//
//			@Override
//			public void mouseClicked(MouseEvent e) {
//				// TODO Auto-generated method stub
//
//			}
//		});
//
//	}
//
//	private class RowTransferHandler extends TransferHandler {
//
//		public RowTransferHandler() {
//			super();
//		}
//
//		@Override
//		public int getSourceActions(JComponent c) {
//			if (c == RowLabel.this) {
//				return COPY;
//			}
//			return NONE;
//		}
//
//		@Override
//		protected Transferable createTransferable(JComponent c) {
//			cc.startedDrag(node);
//			return new Transferable() {
//				@Override
//				public boolean isDataFlavorSupported(DataFlavor flavor) {
//					return flavor.getClass().equals(node.getClass());
//				}
//
//				@Override
//				public DataFlavor[] getTransferDataFlavors() {
//					return new DataFlavor[] { new DataFlavor(node.getClass(), "PON") };
//				}
//
//				@Override
//				public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException, IOException {
//					return node;
//				}
//
//			};
//
//		}
//
//		@Override
//		public boolean canImport(TransferSupport support) {
//			for (DataFlavor df : support.getDataFlavors()) {
//				try {
//					if (node.ed_can_drop(support.getTransferable().getTransferData(df))) {
//						return true;
//					}
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//			return false;
//		}
//
//		@Override
//		public boolean importData(TransferSupport support) {
//			for (DataFlavor df : support.getDataFlavors()) {
//				Object data;
//				try {
//					support.getTransferable();
//					Transferable trns = support.getTransferable();
//					data = trns.getTransferData(df);
//					if (node.ed_can_drop(data)) {
//						node.ed_accept_resource(data, cc);
//					}
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//			return true;
//		}
//		
//		@Override
//		protected void exportDone(JComponent source, Transferable data, int action) {
//			cc.stopedDrag();
//		}
//	}
//
//	@Override
//	public void begunDrag(Object node) {
//		if(this.node.ed_can_drop(node)){
//			SwingUtilities.invokeLater(new Runnable() {
//				
//				@Override
//				public void run() {
//					setBorder(BorderFactory.createLineBorder(new Color(101, 123, 131), 1));
//				}
//			});
//			
//		}
//	}
//
//	@Override
//	public void endedDrag() {
//		SwingUtilities.invokeLater(new Runnable() {
//			@Override
//			public void run() {
//				setBorder(BorderFactory.createEmptyBorder());
//				
//			}
//		});
//	}
//}
