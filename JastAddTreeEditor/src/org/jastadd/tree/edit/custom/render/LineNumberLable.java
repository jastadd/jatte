package org.jastadd.tree.edit.custom.render;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JLabel;

import org.jastadd.tree.edit.ASTNodeInterface;

public class LineNumberLable extends JLabel {
	
		public LineNumberLable(ASTNodeInterface node,int nbrDecimal) {
			int len = String.valueOf(node.ed_line_number()).length();
			String prepend = "";
			for(int i = len;i<nbrDecimal; i++){
				prepend += " ";
			}
			setText(prepend + node.ed_line_number());
			if(node.ed_line_errors().isEmpty()){
				setForeground(Color.gray);
				setFont(new Font("Monospaced", Font.PLAIN, 14));
			}else {
				
				setForeground(Color.red);
				setFont(new Font("Monospaced", Font.BOLD, 14));
				StringBuilder sb = new StringBuilder();
				for(String s : node.ed_line_errors()){
					sb.append(s);
					sb.append('\n');
				}
				setToolTipText(sb.toString());
			}
			
	}

}
