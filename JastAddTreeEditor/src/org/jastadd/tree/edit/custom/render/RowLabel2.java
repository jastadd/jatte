package org.jastadd.tree.edit.custom.render;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.util.TooManyListenersException;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.TransferHandler;

import org.jastadd.tree.edit.ASTNodeInterface;
import org.jastadd.tree.edit.CompilerContainer;
import org.jastadd.tree.edit.DragListner;
import org.jastadd.tree.edit.LabelFormatInterface;

public class RowLabel2 extends JPanel implements DragListner {

	CompilerContainer cc;
	ASTNodeInterface node;
	private StringBuilder sb;

	public RowLabel2(CompilerContainer cc, ASTNodeInterface node) {
		super();
		this.cc = cc;
		this.node = node;
		setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		setAlignmentX(Component.LEFT_ALIGNMENT);
		setBackground(new Color(255, 255, 255));
		setFont(new Font("Monospaced", Font.PLAIN, 14));
		setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));

		sb = new StringBuilder();

		for (LabelFormatInterface lf : node.ed_label_format().getLabelFormats()) {
			this.add(lf.ed_generate_label(cc));
			sb.append(lf.ed_generate_text_label(cc));
		}

		// JLabel label = new JLabel();
		// label.setText(node.ed_label());
		// this.add(label);

		TransferHandler tfh = new RowTransferHandler();
		setTransferHandler(tfh);
		try {
			getDropTarget().addDropTargetListener(new DropTargetListener() {

				@Override
				public void dropActionChanged(DropTargetDragEvent dtde) {
				}

				@Override
				public void drop(DropTargetDropEvent dtde) {
				}

				@Override
				public void dragOver(DropTargetDragEvent dtde) {
				}

				@Override
				public void dragExit(DropTargetEvent dte) {
					if (node.ed_can_drop(cc.currentDragging())) {
						SwingUtilities.invokeLater(new Runnable() {
							@Override
							public void run() {
								setBorder(BorderFactory.createLineBorder(new Color(101, 123, 131), 1));
								// setFont(new Font("Monospaced", Font.PLAIN,
								// 14));
							}
						});
					}
				}

				@Override
				public void dragEnter(DropTargetDragEvent dtde) {
					if (node.ed_can_drop(cc.currentDragging())) {
						SwingUtilities.invokeLater(new Runnable() {
							@Override
							public void run() {
								setBorder(BorderFactory.createLineBorder(new Color(101, 123, 131), 1));
								// setFont(new Font("Monospaced", Font.BOLD,
								// 14));
							}
						});
					}
				}
			});
		} catch (TooManyListenersException e1) {
			e1.printStackTrace();
		}

		addMouseListener(new MouseListener() {

			volatile boolean stopedDrag = false;

			@Override
			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					node.ed_right_click(cc, e);
				}
				stopedDrag = true;
			}

			@Override
			public void mousePressed(MouseEvent e) {
				if (e.isPopupTrigger()) {
					node.ed_right_click(cc, e);
				} else if (SwingUtilities.isLeftMouseButton(e) && node.ed_can_drag()) {

					stopedDrag = false;
					Timer timer = new Timer(300, new ActionListener() {
						@Override
						public void actionPerformed(ActionEvent e2) {
							if (!stopedDrag) {
								SwingUtilities.invokeLater(new Runnable() {
									@Override
									public void run() {
										tfh.exportAsDrag(RowLabel2.this, e, TransferHandler.COPY);
									}
								});
							}
						}
					});
					timer.setRepeats(false);
					timer.start();
				}
			}

			@Override
			public void mouseExited(MouseEvent e) {
				if (cc.currentDragging() != node && !cc.dragging()) {
					SwingUtilities.invokeLater(new Runnable() {
						@Override
						public void run() {
							// setFont(new Font("Monospaced", Font.PLAIN, 14));
							for (LabelFormatInterface lf : node.ed_label_format().getLabelFormats()) {
								// lf.ed_stop_hover(cc);
								setBackground(Color.WHITE);
							}

							RowLabel2.this.revalidate();
						}
					});
				}
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				if (!cc.dragging()) {
					SwingUtilities.invokeLater(new Runnable() {

						@Override
						public void run() {
							// setFont(new Font("Monospaced", Font.BOLD, 14));
							for (LabelFormatInterface lf : node.ed_label_format().getLabelFormats()) {
								// lf.ed_start_hover(cc);
								setBackground(new Color(240, 240, 240));
							}
							RowLabel2.this.revalidate();
						}
					});
				}
			}

			@Override
			public void mouseClicked(MouseEvent e) {
			}

		});

	}

	public String textRep() {
		return sb.toString();
	}

	private class RowTransferHandler extends TransferHandler {

		public RowTransferHandler() {
			super();
		}

		@Override
		public int getSourceActions(JComponent c) {
			if (c == RowLabel2.this) {
				return COPY;
			}
			return NONE;
		}

		@Override
		protected Transferable createTransferable(JComponent c) {
			cc.startedDrag(node);
			Transferable transfer = new Transferable() {
				@Override
				public boolean isDataFlavorSupported(DataFlavor flavor) {
					return flavor.getClass().equals(node.getClass());
				}

				@Override
				public DataFlavor[] getTransferDataFlavors() {
					return new DataFlavor[] { new DataFlavor(node.getClass(), "PON") };
				}

				@Override
				public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException, IOException {
					return node;
				}

			};
			return transfer;

		}

		@Override
		public boolean canImport(TransferSupport support) {
			for (DataFlavor df : support.getDataFlavors()) {
				try {
					if (node.ed_can_drop(support.getTransferable().getTransferData(df))) {
						return true;
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			return false;
		}

		@Override
		public boolean importData(TransferSupport support) {
			for (DataFlavor df : support.getDataFlavors()) {
				Object data;
				try {
					support.getTransferable();
					Transferable trns = support.getTransferable();
					data = trns.getTransferData(df);
					SwingUtilities.invokeLater(new Runnable() {

						@Override
						public void run() {
							if (node.ed_can_drop(data)) {
								node.ed_accept_resource(data, cc);
							}
						}
					});
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			return true;
		}

		@Override
		protected void exportDone(JComponent source, Transferable data, int action) {
			cc.stopedDrag();
		}
	}

	@Override
	public void begunDrag(Object node) {
		if (this.node.ed_can_drop(node)) {
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					setBorder(BorderFactory.createLineBorder(new Color(101, 123, 131), 1));
				}
			});
		}
	}

	@Override
	public void endedDrag() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
			}
		});
	}
}
