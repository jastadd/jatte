package org.jastadd.tree.edit.custom.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputMethodEvent;
import java.awt.event.InputMethodListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.jastadd.tree.edit.ASTNodeInterface;
import org.jastadd.tree.edit.CompilerContainer;
import org.jastadd.tree.edit.Editors;
import org.jastadd.tree.edit.LabelFormatInterface;


public class TokenJTextField extends JTextField {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TokenJTextField(String text, String token, ASTNodeInterface node, CompilerContainer cc ) {
		super(text);
		setFont(new Font("Monospaced", Font.ITALIC, 14));
		setBorder(BorderFactory.createEmptyBorder());
		setBackground(null);
		
		
		setMaximumSize(getPreferredSize());
//		addActionListener(new ActionListener() {
//			@Override
//			public void actionPerformed(ActionEvent e) {
//			//	astText.ed_perform(cc, getText());
//			}
//		});
		
		addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						getParent().dispatchEvent(e);
					}
				});
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						getParent().dispatchEvent(e);
					}
				});
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						getParent().dispatchEvent(e);
					}
				});
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						getParent().dispatchEvent(e);
					}
				});
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						getParent().dispatchEvent(e);
					}
				});
			}
		});
		
		getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void removeUpdate(DocumentEvent e) {
				setBackground(Color.LIGHT_GRAY);
				setMaximumSize(getPreferredSize());
				getParent().revalidate();
			}
			@Override
			public void insertUpdate(DocumentEvent e) {
				setBackground(Color.LIGHT_GRAY);
				setMaximumSize(getPreferredSize());
				getParent().revalidate();
			}
			@Override
			public void changedUpdate(DocumentEvent e) {
				setBackground(Color.LIGHT_GRAY);
				setMaximumSize(getPreferredSize());
				getParent().revalidate();
			}
		});
	}

}
