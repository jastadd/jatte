package org.jastadd.tree.edit;

import java.awt.Component;

public interface LabelFormatInterface extends ASTNodeInterface {
	
	Component ed_generate_label(CompilerContainer cc);
	
	String ed_generate_text_label(CompilerContainer cc);
	
	void ed_start_hover(CompilerContainer cc);

	void ed_stop_hover(CompilerContainer cc);

}
