package org.jastadd.tree.edit;

public interface DragListner {
	
	public void begunDrag(Object node);
	
	public void endedDrag();
	
}
