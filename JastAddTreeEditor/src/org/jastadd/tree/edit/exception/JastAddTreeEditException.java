package org.jastadd.tree.edit.exception;

public class JastAddTreeEditException extends RuntimeException{
	
	private static final long serialVersionUID = -7089196479561884655L;

	public JastAddTreeEditException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public JastAddTreeEditException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public JastAddTreeEditException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public JastAddTreeEditException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public JastAddTreeEditException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
