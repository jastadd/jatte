//package org.jastadd.tree.edit.history;
//
//import static org.junit.Assert.*;
//
//import org.junit.Test;
//
//public class TestHistory {
//
//	@Test
//	public void testInit() {
//		History<String> his = new History<>("start");
//		assertEquals("start",his.getPrevious());
//	}
//	
//	@Test
//	public void testHistoryOne() {
//		History<String> his = new History<>("start");
//		his.setCurrent("next");
//		assertEquals("start",his.getPrevious());
//		assertEquals("start",his.getPrevious());
//	}
//	
//	@Test
//	public void testHistoryMany() {
//		History<String> his = new History<>("start");
//		his.setCurrent("1");
//		his.setCurrent("2");
//		his.setCurrent("3");
//		his.setCurrent("4");
//		assertEquals("3",his.getPrevious());
//		assertEquals("2",his.getPrevious());
//		assertEquals("1",his.getPrevious());
//		assertEquals("start",his.getPrevious());
//	}
//
//	@Test
//	public void testHistoryMulti() {
//		History<String> his = new History<>("start");
//		his.setCurrent("1");
//		his.setCurrent("2");
//		his.setCurrent("3");
//		his.setCurrent("4");
//		assertEquals("3",his.getPrevious());
//		his.setCurrent("4");
//		assertEquals("3",his.getPrevious());
//		assertEquals("2",his.getPrevious());
//		assertEquals("1",his.getPrevious());
//		assertEquals("start",his.getPrevious());
//	}
//	
//	@Test
//	public void testHistoryMulti2() {
//		History<String> his = new History<>("start");
//		his.setCurrent("1");
//		his.setCurrent("2");
//		his.setCurrent("3");
//		his.setCurrent("4");
//		assertEquals("3",his.getPrevious());
//		his.setCurrent("4");
//		assertEquals("3",his.getPrevious());
//		assertEquals("2",his.getPrevious());
//		his.setCurrent("4");
//		his.setCurrent("5");
//		assertEquals("4",his.getPrevious());
//		assertEquals("2",his.getPrevious());
//		assertEquals("1",his.getPrevious());
//		assertEquals("start",his.getPrevious());
//	}
//	@Test
//	public void testHistoryMultiShort() {
//		History<String> his = new History<>("start");
//		his.setCurrent("1");
//		his.setCurrent("2");
//		assertEquals("1",his.getPrevious());
//		his.setCurrent("3");
//		assertEquals("1",his.getPrevious());
//		assertEquals("start",his.getPrevious());
//	}
//}
