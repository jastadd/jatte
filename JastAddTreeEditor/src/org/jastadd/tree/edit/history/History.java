package org.jastadd.tree.edit.history;

import java.util.Deque;
import java.util.concurrent.LinkedBlockingDeque;

public class History<E> {
	
	E current;
	
	Deque<E> history = new LinkedBlockingDeque<E>();
	
	public History(E init) {
		history.push(init);
	}

	public E getPrevious() {
		if(history.size()>1){
			current = history.peek();
			return history.pop();
		}else {
			current = null;
			return history.peek();
		}
		
	}

	public void setCurrent(E current) {
		if(this.current != null){
			history.push(this.current);
		}
		this.current = current;
	}
}
