package org.jastadd.tree.edit;

public interface LabelFormatLineInterface extends ASTNodeInterface {
	
	Iterable<? extends LabelFormatInterface> getLabelFormats();

}
