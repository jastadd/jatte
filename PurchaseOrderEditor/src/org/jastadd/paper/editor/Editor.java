package org.jastadd.paper.editor;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import org.jastadd.paper.editor.gen.PurchaseOrder;
import org.jastadd.tree.edit.CompilerContainer;
import org.jastadd.tree.edit.SubclassExtractor;
import org.jastadd.tree.edit.jtree.MainJtreeContatiner;
import org.jastadd.tree.edit.xml.JastAddXMLParser;
import org.jastadd.tree.edit.xml.JastAddXMLSerializer;

import internal.org.xmlpull.v1.XmlPullParserException;

public class Editor {

	private JFrame frame;
	private MainJtreeContatiner mjc;
	
	public static Object DrAST_root_node;

	public Editor() throws IOException {
		frame = new JFrame("FrameDemo");

		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		SubclassExtractor se = new SubclassExtractor("dir.txt");
		PurchaseOrder po = new PurchaseOrder();
		se.inferAll(po);

		String root = JastAddXMLSerializer.serializ(po);

		mjc = new MainJtreeContatiner(root.getBytes(), new CompilerContainer(se),
				"org.jastadd.paper.editor.gen");

		frame.getContentPane().add(mjc, BorderLayout.CENTER);
		frame.setJMenuBar(new MyMenu());

		frame.pack();

		frame.setVisible(true);
	}

	public void newEditorFromFile(File file) {
		frame.getContentPane().removeAll();
		RandomAccessFile f;
		byte[] data = null;
		try {
			f = new RandomAccessFile(file, "r");
			data = new byte[(int) f.length()];
			f.read(data);
		} catch (IOException e) {
			e.printStackTrace();
		}
		mjc = new MainJtreeContatiner(data, new CompilerContainer(new SubclassExtractor("dir.txt")),
				"org.jastadd.paper.editor.gen");
		frame.getContentPane().add(mjc, BorderLayout.CENTER);
		frame.revalidate();
	}

	public void saveEditorToFile(File file) {
			try {
				FileOutputStream f = new FileOutputStream(file);
				f.write(mjc.getData());
				f.flush();
				f.close();
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		

	}

	public static void main(String[] args) throws IOException {
		if(args.length == 0){
			new Editor();
		}else {
			RandomAccessFile f;
			byte[] data = null;
			try {
				f = new RandomAccessFile(args[0], "r");
				data = new byte[(int) f.length()];
				f.read(data);
			} catch (IOException e) {
				e.printStackTrace();
			}
			try {
				DrAST_root_node = JastAddXMLParser.parse(new String(data), "org.jastadd.paper.editor.gen");
			} catch (XmlPullParserException e) {
				e.printStackTrace();
			}	
		}
	}

	class MyMenu extends JMenuBar {
		final JFileChooser fc = new JFileChooser();

		public MyMenu() {
			JMenu menu = new JMenu("File");
			add(menu);
			JMenuItem jmi = new JMenuItem("Open");
			jmi.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					int status = fc.showOpenDialog(jmi);
					if (status == JFileChooser.APPROVE_OPTION) {
						File f = fc.getSelectedFile();
						newEditorFromFile(f);
					}

				}
			});
			menu.add(jmi);
			JMenuItem jms = new JMenuItem("Save");
			jms.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					int status = fc.showSaveDialog(jms);
					if (status == JFileChooser.APPROVE_OPTION) {
						File f = fc.getSelectedFile();
						saveEditorToFile(f);
					}

				}
			});
			menu.add(jms);
		}

	}

}
