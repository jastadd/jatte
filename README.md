# README #

To run the application:

* Check out the code
* ```cd CalcASTEditor/```
* ```ant build-jar```
* ```java -jar CalcASTEditor.jar```



### Who do I talk to? ###

* Alfred �kesson: alfred.akesson@cs.lth.se

### Licence ###

Most of the code is licensed under The 3-Clause BSD License, according to ```license.txt```, but folder jatte/JastAddTreeEditor/resource use other licenses. 
